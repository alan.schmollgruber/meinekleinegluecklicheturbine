package de.awacademy.meineKleineGluecklicheTurbine.entitiesTest;
import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class ControllerEntityTest {
    private static EntityManager entityManager;

    @Before
    public void setUp() throws Exception {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Test
    public void checkLogIn (){
        ControllerEntity controllerModel = new ControllerEntity();
        controllerModel.setLogin("test");
        entityManager.getTransaction().begin();
        entityManager.persist(controllerModel);
        entityManager.getTransaction().commit();

        Long id= controllerModel.getId();
        ControllerEntity test = entityManager.find(ControllerEntity.class, id);
        String string = test.getLogin();

        Assert.assertEquals("test", string);

    }

}
