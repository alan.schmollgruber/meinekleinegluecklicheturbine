package de.awacademy.meineKleineGluecklicheTurbine.entitiesTest;
import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class TechnicianEntityTest {
    private static EntityManager entityManager;

    @Before
    public void setUp() throws Exception {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Test
    public void checkLogIn (){
        TechnicianEntity controllerModel = new TechnicianEntity();
        controllerModel.setName("test");
        entityManager.getTransaction().begin();
        entityManager.persist(controllerModel);
        entityManager.getTransaction().commit();

        Long id= controllerModel.getId();
        TechnicianEntity test = entityManager.find(TechnicianEntity.class, id);
        String string = test.getName();

        Assert.assertEquals("test", string);

    }
}
