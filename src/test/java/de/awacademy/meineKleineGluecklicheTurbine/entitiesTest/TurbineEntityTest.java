package de.awacademy.meineKleineGluecklicheTurbine.entitiesTest;
import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class TurbineEntityTest {
    private static EntityManager entityManager;

    @Before
    public void setUp() throws Exception {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @Test
    public void verifyInformationFromTurbine (){
        TurbineEntity turbineEntity = new TurbineEntity();
       turbineEntity.setSerialID("ID456");
        entityManager.getTransaction().begin();
        entityManager.persist(turbineEntity);
        entityManager.getTransaction().commit();

        Long id= turbineEntity.getId();
        TurbineEntity test = entityManager.find(TurbineEntity.class, id);
        String string = test.getSerialID();

        Assert.assertEquals("ID456", string);

    }
}
