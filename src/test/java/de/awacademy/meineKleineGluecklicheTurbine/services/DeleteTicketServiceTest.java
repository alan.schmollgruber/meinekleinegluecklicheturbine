package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class DeleteTicketServiceTest {
        private DeleteTicketRepositoryTest deleteTicketRepository;
        private TurbineTicketEntity turbineTicketEntity;
        private TurbineTicketRepositoryTest turbineTicketRepository;

    @Before
        public void setUp()  {
            deleteTicketRepository = new DeleteTicketRepositoryTest();
            turbineTicketEntity = new TurbineTicketEntity();
            turbineTicketRepository = new TurbineTicketRepositoryTest();
        }


        @Test
        public void deleteSelectedUserTest(){
            turbineTicketEntity.setSerialID("DeleteTicketServiceTest");
            turbineTicketRepository.createNewTicket(turbineTicketEntity);
            // System.out.println(turbineTicketEntity.getId() + " " + turbineTicketEntity.getSerialID());

            List<TurbineTicketEntity> allTickets1 = turbineTicketRepository.findAll();
            for (TurbineTicketEntity ticketEntity : allTickets1) {
                System.out.println(ticketEntity.getSerialID());
                if (ticketEntity.getSerialID() != null && ticketEntity.getSerialID().equals("DeleteTicketServiceTest")) {
                    turbineTicketEntity = ticketEntity;
                }
            }
            // System.out.println(turbineTicketEntity.getId() + " " + turbineTicketEntity.getSerialID());

            deleteTicketRepository.findTicketToDelete(turbineTicketEntity.getId());

            List<TurbineTicketEntity> allTickets2 = turbineTicketRepository.findAll();
            for (TurbineTicketEntity ticketEntity : allTickets2) {
                if (ticketEntity.getId().equals("DeleteTicketServiceTest")) {
                    Assert.assertFalse(false);
                }
            }
        }
}
