package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.CreateUserRespository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryController;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryTechnician;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryTechnicianTest;
import org.junit.Assert;
import org.junit.Test;

public class LoginServiceTechnicianTest {

    @Test
    public void loginCheckTechnicianTest() {

        CreateUserRespository createUserRespository = new CreateUserRespository();

        LoginRepositoryTechnicianTest loginRepositoryTechnician = new LoginRepositoryTechnicianTest();

        TechnicianEntity technicianEntity = createUserRespository.createUserTechnician("TestName","testSurname","123");
        System.out.println(technicianEntity.getName());

        TechnicianEntity technicianEntity1= loginRepositoryTechnician.loginCheckTechnicians("TestName","123");

        Assert.assertEquals(technicianEntity.getName(),technicianEntity1.getName() );
    }
}
