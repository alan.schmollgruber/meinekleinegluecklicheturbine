package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;
import de.awacademy.meineKleineGluecklicheTurbine.mapper.TurbineReportMapper;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineModel;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineReportModel;
import de.awacademy.meineKleineGluecklicheTurbine.reportingBusinessLogic.TurbineDiagnosticReporting;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineReportingRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineTicketRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import static de.awacademy.meineKleineGluecklicheTurbine.reportingBusinessLogic.TurbineDiagnosticReporting.DEST;

public class TurbineReportingTest {

    private TurbineReportingRepository turbineReportingRepository;
    private TurbineReportMapper turbineReportMapper;
    private TurbineNewTicketService turbineNewTicketService;
    private TurbineReportingEntity turbineReportingEntity;

    @Before
    public void init() {
        turbineReportingRepository = new TurbineReportingRepository();
        turbineReportMapper = new TurbineReportMapper();
        turbineNewTicketService = new TurbineNewTicketService();
        turbineReportingEntity = new TurbineReportingEntity();
    }

    @Test
    public void findOneTurbine() {
        List<TurbineReportingEntity> allTurbines = turbineReportingRepository.findAll();
        String ownSerialID = " TEST ";
        turbineReportingEntity.setSerialID(ownSerialID);
        allTurbines.add(turbineReportingEntity);

        for (TurbineReportingEntity turbineReportingEntity : allTurbines) {

            String entitySerial = turbineReportingEntity.getSerialID().substring(1).trim();
            String entitySerial2 = turbineReportingEntity.getSerialID().trim();

            boolean serialEquality = entitySerial.equals(ownSerialID.trim()) || entitySerial2.equals(ownSerialID);
            //System.out.println("Test Reporting Entity List: " +serialEquality + " " + serialId + " "+ entitySerial + " "+ entitySerial2);
            if (serialEquality) {
                Assert.assertTrue(serialEquality);
            }
        }
    }

    @Ignore
    @Test
    public void generateOneReport(){
        List<TurbineReportingEntity> allTurbines = turbineReportingRepository.findAll();
        String ownSerialID = "TestForReporting";
        turbineReportingEntity.setSerialID(ownSerialID);
        turbineReportingEntity.setSerialID(ownSerialID);
        allTurbines.add(turbineReportingEntity);
        try {
            //define timestamp related information
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
            LocalDateTime localDateTimeStamp = LocalDateTime.now();
            Date date = new Date();

            String dateTimeStamp = dateTimeFormatter.format(localDateTimeStamp);

            //create new turbineDiagnosticReporting-file instance
            TurbineDiagnosticReporting turbineDiagnosticReporting = new TurbineDiagnosticReporting("repairReport"+dateTimeStamp+".pdf");
            File file = new File(DEST);
            file.getParentFile().mkdirs();

            //content input and pdf generation
            TurbineReportModel turbineReportModel = new TurbineReportModel();
            turbineDiagnosticReporting.importDocumentContent(localDateTimeStamp, turbineReportModel);
            turbineDiagnosticReporting.createPdf();

            //log
            System.out.println("\nTest generate report to path: ...\n..."+DEST);

            Assert.assertTrue(true);

        } catch (IOException e){
            System.out.println("IOException !");
            Assert.assertFalse(false);
        }
    }
}
