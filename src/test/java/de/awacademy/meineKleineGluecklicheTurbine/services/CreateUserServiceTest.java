package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.controllers.AdminController;
import de.awacademy.meineKleineGluecklicheTurbine.entities.AdminEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.CreateUserRespository;
import org.junit.Assert;
import org.junit.Test;

public class CreateUserServiceTest {


    @Test
    public void createTechnicianTest(){
        CreateUserRespository createUserRespository = new CreateUserRespository();
        AdminController adminController = new AdminController();
        adminController.setName("testName");
        adminController.setSurName("testSurname");
        adminController.setPassword("123");

        TechnicianEntity technicianEntity = createUserRespository.createUserTechnician(adminController.getName(), adminController.getPassword(), adminController.getSurName());

        Assert.assertEquals("testName",technicianEntity.getName() );

    }

    @Test
    public void createControllerTest(){
        CreateUserRespository createUserRespository = new CreateUserRespository();
        AdminController adminController = new AdminController();
        adminController.setName("testName");
        adminController.setSurName("testSurname");
        adminController.setPassword("123");

        ControllerEntity controllerEntity =  createUserRespository.createUserController(adminController.getName(), adminController.getPassword(), adminController.getSurName());

        Assert.assertEquals("testName",controllerEntity.getName() );

    }

    @Test
    public void createAdminTest(){
        CreateUserRespository createUserRespository = new CreateUserRespository();
        AdminController adminController = new AdminController();
        adminController.setName("testName");
        adminController.setSurName("testSurname");
        adminController.setPassword("123");

        AdminEntity adminEntity =  createUserRespository.createUserAdmin(adminController.getName(), adminController.getPassword(), adminController.getSurName());

        Assert.assertEquals("testName",adminEntity.getName() );

    }



}
