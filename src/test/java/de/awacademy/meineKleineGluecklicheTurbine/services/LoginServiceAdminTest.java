package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.AdminEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.models.UserModel;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.CreateUserRespository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryAdmin;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryAdminTest;
import de.awacademy.meineKleineGluecklicheTurbine.services.CreateUserService;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class LoginServiceAdminTest {



    @Test
    public void loginCheckAdminsTest() {


        CreateUserRespository createUserRespository = new CreateUserRespository();

        AdminEntity adminEntity = createUserRespository.createUserAdmin("TestName","TestSurname","123");
        System.out.println(adminEntity.getName());

        LoginRepositoryAdminTest loginRepositoryAdmin =new LoginRepositoryAdminTest();

       AdminEntity adminEntity1= loginRepositoryAdmin.loginCheckAdmin("TestName","123");

        Assert.assertEquals(adminEntity.getName(),adminEntity1.getName() );
        }

    }

