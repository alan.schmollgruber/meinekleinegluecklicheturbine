package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.DeleteUserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DeleteUserServiceTest {

    @Before
    public void setUp()  {
        TechnicianEntity technicianEntity = new TechnicianEntity();
        technicianEntity.setPosition("Technician");
    }


    @Test
    public void deleteSelectedUserTest(){
        TechnicianEntity technicianEntity = new TechnicianEntity();
        technicianEntity.setPosition("Technician");

        DeleteUserRepository deleteUserRespository = new DeleteUserRepository();

        deleteUserRespository.findUserToDelete(technicianEntity.getId(), technicianEntity.getPosition());

        Assert.assertEquals(null,technicianEntity.getName() );
    }

}
