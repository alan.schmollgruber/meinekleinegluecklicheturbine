package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;
import de.awacademy.meineKleineGluecklicheTurbine.mapper.TurbineTicketMapper;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineTicketModel;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineTicketRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineTicketRepositoryTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ejb.EJB;
import java.util.List;

public class TurbineTicketServiceTest {

    @Test
    public void getAllTurbinesTest(){
        TurbineTicketRepositoryTest turbineTicketRepository = new TurbineTicketRepositoryTest();
        List<TurbineTicketEntity> turbineTicketModels = turbineTicketRepository.findAll();
        Assert.assertTrue(turbineTicketModels.size()>=0);
    }
}
