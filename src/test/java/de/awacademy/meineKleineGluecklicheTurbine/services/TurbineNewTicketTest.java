package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineTicketRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineTicketRepositoryTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TurbineNewTicketTest {

    private TurbineTicketRepositoryTest turbineTicketRepository;
    private TurbineTicketEntity turbineTicketEntity;

    @Before
    public void init() {
        turbineTicketRepository = new TurbineTicketRepositoryTest();
        turbineTicketEntity = new TurbineTicketEntity();
    }

    @Test
    public void newTurbineTicketIntoDB() {
        turbineTicketEntity.setSerialID("TESTTESTTEST");
        turbineTicketRepository.createNewTicket(turbineTicketEntity);
        List<TurbineTicketEntity> allTickets = turbineTicketRepository.findAll();

        for (TurbineTicketEntity ticketEntity : allTickets) {
            if (ticketEntity.getSerialID().equals("TESTTESTTEST")) {
                Assert.assertTrue(true);
            }
        }
    }
}
