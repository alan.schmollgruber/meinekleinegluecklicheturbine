package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineRepositoryTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


import java.util.List;

public class ReportingTest {

    private TurbineReportingEntity turbineReportingEntity;
    private TurbineRepositoryTest turbineRepository;
    @Before
    public void init() {
        turbineReportingEntity = new TurbineReportingEntity();
        turbineRepository = new TurbineRepositoryTest();
    }

    @Test
    public void findOneExsistingTurbine() {
        List<TurbineReportingEntity> allTurbines = turbineRepository.findAll();
        turbineReportingEntity.setSerialID("TESTTEST");
        allTurbines.add(turbineReportingEntity);

        for(TurbineReportingEntity turbineEntity:allTurbines){
            if(turbineEntity.getSerialID().equals("TESTTEST")){
                Assert.assertTrue(true);
            }

        }

    }

    @Test
    public void findNotExsistingTurbine() {
        List<TurbineReportingEntity> allTurbines = turbineRepository.findAll();

        for(TurbineReportingEntity turbineEntity:allTurbines){
            if(turbineEntity.getSerialID().equals("NOOOOO")){
                Assert.assertFalse(false);
            }

        }

    }

}

