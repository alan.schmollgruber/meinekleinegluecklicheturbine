package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.AdminEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.CreateUserRespository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryAdmin;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryController;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryControllerTest;
import org.junit.Assert;
import org.junit.Test;

public class LoginServiceControllerTest {

    @Test
    public void loginCheckControllersTest() {

        CreateUserRespository createUserRespository = new CreateUserRespository();

        LoginRepositoryControllerTest loginRepositoryController = new LoginRepositoryControllerTest();

        ControllerEntity controllerEntity = createUserRespository.createUserController("TestName","testSurname","123");
            System.out.println(controllerEntity.getName());

            ControllerEntity controllerEntity1= loginRepositoryController.loginCheckController("TestName","123");

            Assert.assertEquals(controllerEntity.getName(),controllerEntity1.getName() );
        }
    }

