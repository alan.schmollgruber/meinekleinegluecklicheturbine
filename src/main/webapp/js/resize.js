function calculateAreas(turbineCoords, turbineData) {
    let map = document.getElementById("map");

    sessionStorage.OUTSIDE_AREA = "4,5,1,2701,1996,2699,1996,7,1600,76,1785,355,1974,1203,1974,1443,1460,1660,1549,1927,1808,2155,1778,2279,1577,2409,1658,2595,1616,2641,1462,2588,1167,2688,1034,2637,1011,2680,916,2680,888,2634,614,2589,310,2619,317,2370,458,2128,126,2044,52,1893,29,1690,24,1365,56,1261,20,1074,176,1065,255,929,185,878,193,811,269,809,315,611,257,592,319,447,579,459,646,314,598,261,520,32,879,60,1047,253,1126,159,1199,214,1117,341,1321,276,1405,178,1501,187,1552,111,1974,7";

    sessionStorage.BRLN_AREA = "1637,816,1670,871,1693,890,1683,917,1626,911,1566,910,1566,841";
    sessionStorage.BRMN_AREA = "589,639,686,669,683,708,637,698,617,666";
    sessionStorage.BRNDNBRG_AREA = "1169,677,1356,756,1365,880,1393,940,1375,1003,1573,1104,1600,1254,1780,1235,1925,1160,1868,883,1877,830,1769,710,1830,592,1752,602,1778,544,1683,523,1607,615,1478,641,1358,592,1633,813,1693,887,1683,920,1564,908,1570,844,1635,814,1372,586";
    sessionStorage.BW_AREA = "332,2601,344,2398,494,2113,561,2004,549,1901,665,1938,752,1854,851,1857,900,1942,922,1921,938,2043,1006,2131,1005,2219,915,2293,937,2561,803,2591,683,2512,720,2561,570,2582";
    sessionStorage.HSSN_AREA = "413,1726,492,1644,460,1576,529,1448,623,1369,595,1307,794,1189,837,1213,812,1282,852,1298,875,1265,953,1357,879,1526,918,1540,872,1616,791,1715,679,1717,704,1897,646,1927,547,1893,559,1833,510,1740,427,1756";
    sessionStorage.MCKLNBRG_AREA = "1176,677,1100,599,1026,586,1098,484,1056,422,1119,362,1201,394,1245,311,1434,226,1589,110,1764,382,1819,576,1686,507,1614,618,1485,636,1338,588";
    sessionStorage.NDRSCHSN_AREA = "739,412,847,521,897,567,1095,600,1229,703,1066,773,1125,925,1123,1022,1026,1054,1052,1186,837,1289,826,1199,771,1178,780,1111,692,999,720,888,635,918,619,874,549,900,581,999,471,1038,471,934,406,902,286,960,211,872,218,821,284,821,328,589,273,581,294,472";
    sessionStorage.NRW_AREA = "715,890,696,998,782,1187,595,1309,629,1360,484,1505,425,1420,178,1616,104,1606,14,1367,90,1263,40,1104,229,1054,213,1016,402,901,466,934,459,1044,582,1000,545,901,623,880,635,920,670,922";
    sessionStorage.RHNLDPFLZ_AREA = "86,1881,33,1666,188,1591,427,1431,494,1549,466,1662,425,1749,511,1736,554,1901,519,2097,340,2052,285,1897,218,1860,155,1893";
    sessionStorage.SCHLSWG_AREA = "1024,590,1100,482,1059,417,1086,382,1056,348,1121,295,1167,217,1116,175,1054,261,911,187,895,102,828,65,573,14,619,253,695,399,759,417,844,521,932,459,962,562";
    sessionStorage.SCHSN_AREA = "1397,1365,1374,1217,1560,1173,1597,1252,1774,1249,1795,1192,1988,1188,1992,1402,1925,1432,1890,1360,1831,1367,1863,1410,1425,1673,1317,1577,1411,1516,1397,1463,1485,1420,1448,1379";
    sessionStorage.SCHSNNHLT_AREA = "1397,1415,1377,1221,1584,1155,1572,1095,1372,1002,1386,883,1363,760,1239,710,1068,767,1123,1021,1029,1058,1045,1182,1218,1307,1231,1360";
    sessionStorage.SRLND_AREA = "84,1890,211,1874,289,1893,314,1984,287,2045,167,2042";
    sessionStorage.THRNGN_AREA = "881,1524,951,1346,884,1286,1054,1192,1121,1265,1223,1302,1222,1357,1388,1418,1413,1362,1480,1420,1395,1470,1404,1523,1319,1604,1202,1584,1167,1655,1056,1674";
    sessionStorage.HMBRG_AREA = "854,509,939,466,960,576,868,563";
    sessionStorage.BYRN_AREA = "704,1902,682,1716,793,1707,935,1557,1062,1668,1081,1610,1173,1647,1190,1575,1248,1614,1331,1603,1471,1766,1449,1840,1780,2154,1760,2248,1706,2223,1658,2326,1541,2396,1626,2561,1605,2614,1534,2543,1423,2532,1140,2654,997,2591,1010,2662,950,2693,817,2589,936,2550,913,2289,1008,2197,921,1922,799,1839";

    sessionStorage.BRLN_AREA_COORDS = "";
    sessionStorage.BRMN_AREA_COORDS = "";
    sessionStorage.BRNDNBRG_AREA_COORDS = "";
    sessionStorage.BW_AREA_COORDS = "";
    sessionStorage.HSSN_AREA_COORDS = "";
    sessionStorage.MCKLNBRG_AREA_COORDS = "";
    sessionStorage.NDRSCHSN_AREA_COORDS = "";
    sessionStorage.NRW_AREA_COORDS = "";
    sessionStorage.RHNLDPFLZ_AREA_COORDS = "";
    sessionStorage.SCHLSWG_AREA_COORDS = "";
    sessionStorage.SCHSN_AREA_COORDS = "";
    sessionStorage.SCHSNNHLT_AREA_COORDS = "";
    sessionStorage.SRLND_AREA_COORDS = "";
    sessionStorage.THRNGN_AREA_COORDS = "";
    sessionStorage.HMBRG_AREA_COORDS = "";
    sessionStorage.BYRN_AREA_COORDS = "";


    $("#1").attr("coords", calculateArea(map, sessionStorage.BRLN_AREA));
    $("#2").attr("coords", calculateArea(map, sessionStorage.BRMN_AREA));
    $("#3").attr("coords", calculateArea(map, sessionStorage.BRNDNBRG_AREA));
    $("#4").attr("coords", calculateArea(map, sessionStorage.BW_AREA));
    $("#5").attr("coords", calculateArea(map, sessionStorage.HSSN_AREA));
    $("#6").attr("coords", calculateArea(map, sessionStorage.MCKLNBRG_AREA));
    $("#7").attr("coords", calculateArea(map, sessionStorage.NDRSCHSN_AREA));
    $("#8").attr("coords", calculateArea(map, sessionStorage.NRW_AREA));
    $("#9").attr("coords", calculateArea(map, sessionStorage.RHNLDPFLZ_AREA));
    $("#10").attr("coords", calculateArea(map, sessionStorage.SCHLSWG_AREA));
    $("#11").attr("coords", calculateArea(map, sessionStorage.SCHSN_AREA));
    $("#12").attr("coords", calculateArea(map, sessionStorage.SCHSNNHLT_AREA));
    $("#13").attr("coords", calculateArea(map, sessionStorage.SRLND_AREA));
    $("#14").attr("coords", calculateArea(map, sessionStorage.THRNGN_AREA));
    $("#15").attr("coords", calculateArea(map, sessionStorage.HMBRG_AREA));
    $("#16").attr("coords", calculateArea(map, sessionStorage.BYRN_AREA));

    $("#outskirts").attr("coords", calculateArea(map, sessionStorage.OUTSIDE_AREA));


    $("#testPinGen").attr("coords", calculateArea(map, "1000,1000,20"));


    pinManager(turbineCoords, turbineData);

}

function pinManager(turbineCoords, turbineData) {
    ///NOVAJA HUYNYA
    let map = document.getElementById("map");

    turbineCoords = turbineCoords.split(";");
    turbineData = turbineData.split(";");

    let mapState = document.getElementById("map").getAttribute("usemap");
    console.log("map b4 trim " + mapState)
    mapState = mapState.substring(1,mapState.length-4);
    console.log(mapState)

    for (let i = 0; i < turbineCoords.length; i++) {
        let dataThisTurbine = turbineData[i].split(",");
        let turbineCoordsGeneral = turbineCoords[i].split("#");
        let coordz = turbineCoordsGeneral[0] + ",30";
        let ID = "pin_" + i;
        let selector = "#" + ID;
        let state = mapStates(dataThisTurbine[0]);
        console.log("vergl. mapstate" + state);
        let status = mapStatus(dataThisTurbine[1]);
        let newPinArea = "<area class=\"pin " + status + "\" id=\"" + ID +"\" alt=\"" + state + "\" title=\"" + dataThisTurbine[2] + "\" href=\"#popup\" coords=\"" + coordz + "\" shape=\"circle\">";

        if(!$(selector).length){
            $("#1").before(newPinArea);
        }

        $(selector).attr("coords", calculateArea(map, coordz));

        if (state === mapState) {
            console.log("Compare " + state + " " + mapState);

            let coordzState = turbineCoordsGeneral[1] + ",30";
            let IDState = "pin_" + i + "_" + state;
            let selectorStatePin = "#" + IDState;
            let selectorMap = "#" + state + "_map";
            console.log(selectorMap)
            let newPinAreaState = "<area class=\"pin " + status + " " + state + "\" id=\"" + IDState +"\" alt=\"" + state + "\" title=\"" + dataThisTurbine[2] + "\" href=\"#popup\" coords=\"" + coordzState + "\" shape=\"circle\">";
            if(!$(selectorStatePin).length){
                $(selectorMap).prepend(newPinAreaState);
            }

            $(selectorStatePin).attr("coords", calculateArea(map, coordzState));
        }

    }

    function mapStatus(status) {
        switch(status) {
            case "1":
                return "OK";
            break; case "2":
                return "CHECK";
            break; case "3":
                return "NOK";
        }
    }
}


function mapStates(State) {
    switch(State) {
        case "Berlin":
            return "BRLN";
            break;case "Bremen":
            return "BRMN";
            break;case "Brandenburg":
            return "BRNDNBRG";
            break;case "BadenWuerttemberg":
            return "BW";
            break;case "Bayern":
            return "BYRN";
            break;case "Hamburg":
            return "HMBRG";
            break;case "Hessen":
            return "HSSN";
            break;case "MacklenburgVorpommern":
            return "MCKLNBRG";
            break;case "Niedersachsen":
            return "NDRSCHSN";
            break;case "Nordrhein-Westfallen":
            return "NRW";
            break;case "Rheinland-Pfalz":
            return "RHNLDPFLZ";
            break;case "Schleswig-Holstein":
            return "SCHLSWG";
            break;case "Sachsen":
            return "SCHSN";
            break;case "SachsenAnhalt":
            return "SCHSNNHLT";
            break;case "Saarland":
            return "SRLND";
            break;case "Thuringen":
            return "THRNGN";
            break;
        default:
        // code block
    }
}

function calculateArea(map, coords) {
    coords = coords.split(",");
    for (let i = 0; i < coords.length; i++) {
        coords[i] = Math.round(coords[i]*map.width/sessionStorage.defaultWidth );
    }
    return coords.toString();
}

function composeImage(src) {
    let mapImage = new Image();
    mapImage.src = src;

    let canvas = document.createElement("canvas");

    canvas.width  = sessionStorage.defaultWidth;
    canvas.height = sessionStorage.defaultHeight;


    let context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
    console.log(canvas.width + " " + canvas.height)

    mapImage.onload = function () {
        console.log("drawing " + src)
        context.drawImage(mapImage, 0, 0);
        console.log("drawn")

        if(document.getElementById("AllTurbines").checked) {
            ///NOVAYA HUYNYA
            drawPins(context, mapImage, src, "ALL");
        }/* else
            if(document.getElementById("CHECK").checked) {
            ///NOVAYA HUYNYA
            console.log("CHECK");
            drawPins(context, "CHECK");
        } else
            if(document.getElementById("NOK").checked) {
            ///NOVAYA HUYNYA
            console.log("NOK");
            drawPins(context, "NOK");
        }*/
        /*setTimeout(function(){
            $("#map").attr("src", canvas.toDataURL("image/png"));
        }, 100);*/
        $("#map").attr("src", canvas.toDataURL("image/png"));


    };
}

function drawPins(context, mapImage, src, status) {

    var pinOrange = new Image();
    pinOrange.src = "images/turbineIcons/WT_CHECK.png";

    var pinGreen = new Image();
    pinGreen.src = "images/turbineIcons/WT_OK.png";

    var pinRed = new Image();
    pinRed.src = "images/turbineIcons/WT_NOK.png";

    console.log("PINS DEFINED")

    switch (status) {
        case "ALL":
            console.log("br pin choice " + src + "trimmed " + src.substring(0,14))
            if (src.substring(0,14) === "images/regions") {
                console.log("check if inside state " + src.substring(0,14) + "   " + "images/regions");
                let thisState = src.substring(15,src.length-4);
                console.log(thisState)
                let selector = "pin " + thisState;
                var pins = document.getElementsByClassName(selector);

                // console.log(pins.item(0).getAttribute("coords") + "length" + pins.length)


            } else {
                var pins = document.getElementsByClassName("pin");
            }

            break;
        case "OK":
            var pins = document.getElementsByClassName("OK"); break;
        case "CHECK":
            var pins = document.getElementsByClassName("CHECK"); break;
        case "NOK":
            var pins = document.getElementsByClassName("NOK"); break;
    }


    let map = document.getElementById("map");

    let multFactor = sessionStorage.defaultWidth/map.width;
    // context.drawImage(mapImage, 0, 0);

    // console.log("Before pin processing" + pins.item(0).getAttribute("coords") + "pin length " + pins.length)
    if (pins.length > 0) {
        for (let i = 0; i < pins.length; i++) {
            let coords = pins.item(i).getAttribute("coords").split(",");

            console.log("inside for")

            let pinSelector = pins.item(i).getAttribute("class");
            if (pinSelector.substring(4, 6) == "CH") {
                pinSelector = pinSelector.substring(0, 9);
            } else {
                pinSelector = pinSelector.substring(0, 6);
            }
            console.log("PIN SELECTOR = " + pinSelector)

            switch (pinSelector) {
                case "pin OK":
                    console.log("pin OK" + i)
                    var pin = pinGreen;
                    break;
                case "pin CHECK":
                    console.log("pin CHECK" + i)
                    var pin = pinOrange;
                    break;
                case "pin NOK":
                    console.log("pin NOK" + i)
                    var pin = pinRed;
                    break;
            }

            drawPin(context, pin, Math.round(coords[0] * multFactor), Math.round(coords[1] * multFactor), 80);


        }
    }
}

function drawPin(context, img, x, y, size) {
    let xAdjust = size*0.5;
    let yAdjust = size*0.6;

    if (img.complete) {
        context.drawImage(img, x - xAdjust , y - yAdjust, size, size);
        console.log("pin drawn")
    } else {
        img.onload = function () {
            context.drawImage(img, x - xAdjust, y - yAdjust, size, size);
            console.log("pin drawn")
        };
    }
}

