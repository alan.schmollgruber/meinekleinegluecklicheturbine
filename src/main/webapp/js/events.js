function initializeDefaults() {
    sessionStorage.defaultWidth = 2000;
    sessionStorage.defaultHeight = 2706;
}

function checkBoxChange() {
    if($("#map").attr("usemap") == "#general-map") {
        composeImage("images/deutschland.png");
    }
}

$(".State, #outskirts").mouseover(function(){
    let src = "images/" + $(this).attr("alt") + ".png";
    $("#map").attr("src", src);
    composeImage(src);
});

$(".pin").mouseover(function () {
    let src = $("#map").attr("src");
    if($("#map").attr("usemap") == "#general-map") {
        src = "images/" + $(this).attr("alt") + ".png";
        $("#map").attr("src", src);
    }
    composeImage(src);
});

$(".pin").mousedown(function(){
    let getID = "turbine-description-1";
    let selector = "#" + getID;
    let newDiv = "&lt;div class=\"turbine-description\" id=" + getID+ " \">" + "&lt;p>Pezdos&lt;/p>" +"&lt;/div>";

    if(!$(selector).length){
        console.log("add div");
        $("#popup:first-child").before(newDiv);
    }
    $(".turbine-description").hide();
    $(selector).show();
});

window.onresize = function() { calculateAreas();}

$(".State").click(function () {
    $('html,body').scrollTop(0);

    let src = "images/regions/" + $(this).attr("alt") + ".png";
    let usemap = "#State_" + $(this).attr("id") + "_map";

    $("#map").attr("src", src);
    $("#map").attr("usemap", usemap);

    if (!$("#returnBtn").length) {
        let ID = "returnBtn";
        let srcInitial = "images/deutschland.png";
        var newDiv = "&lt;div class=\"returnBtn\" id=\" "+ ID +" \">" + "&lt;input id=\"retBtn\" type=\"button\" value=\"General map\" onclick=\" returnToGeneralMap();\">" + "&lt;/input>" + "&lt;/div>";
        $("#beforeReturn").before(newDiv);

    } else { $("#returnBtn").removeClass("hidden");}

    composeImage(src);
});

function returnToGeneralMap() {
    console.log("SUKAAAAAAAA");
    let srcInitial = "images/deutschland.png";
    let mapInitial = "#general-map";
    $("#map").attr("src", srcInitial);
    $("#map").attr("usemap", mapInitial);
    $("#returnBtn").addClass("hidden");

    composeImage(srcInitial);
}
