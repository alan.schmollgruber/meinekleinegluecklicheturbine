package de.awacademy.meineKleineGluecklicheTurbine.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TechnicianEntity {

    @Id
    @GeneratedValue
    private long id;
    private String login;
    private String name;
    private String surname;
    private String password;
    private String position ="Technician";

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String nameUser) {
        this.name = nameUser;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surnameUser) {
        this.surname = surnameUser;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
