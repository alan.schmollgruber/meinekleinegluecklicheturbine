package de.awacademy.meineKleineGluecklicheTurbine.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class TurbineTicketEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String serialID;
    private String coordinates;
    private String district;
    private int casing;
    private int electronics;
    private int gearbox;
    private int  generator;
    private int rotor;
    private int power;
    private int status;
    private int lastMantained;
    private String state;
    private boolean repaired;

    private Long idTurbine;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTimestamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerialID() {
        return serialID;
    }

    public void setSerialID(String serialID) {
        this.serialID = serialID;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public int getCasing() {
        return casing;
    }

    public void setCasing(int casing) {
        this.casing = casing;
    }

    public int getElectronics() {
        return electronics;
    }

    public void setElectronics(int electronics) {
        this.electronics = electronics;
    }

    public int getGearbox() {
        return gearbox;
    }

    public void setGearbox(int gearbox) {
        this.gearbox = gearbox;
    }

    public int getGenerator() {
        return generator;
    }

    public void setGenerator(int generator) {
        this.generator = generator;
    }

    public int getRotor() {
        return rotor;
    }

    public void setRotor(int rotor) {
        this.rotor = rotor;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getLastMantained() {
        return lastMantained;
    }

    public void setLastMantained(int lastMantained) {
        this.lastMantained = lastMantained;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Long getIdTurbine() {
        return idTurbine;
    }

    public void setIdTurbine(Long idTurbine) {
        this.idTurbine = idTurbine;
    }

    public boolean isRepaired() {
        return repaired;
    }

    public void setRepaired(boolean repaired) {
        this.repaired = repaired;
    }
}
