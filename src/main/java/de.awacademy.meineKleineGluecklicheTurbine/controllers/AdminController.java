package de.awacademy.meineKleineGluecklicheTurbine.controllers;

import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.services.CreateUserService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;
import java.util.ArrayList;

@Named
@RequestScoped
public class AdminController {
    @EJB
    private CreateUserService adminService;

    private String name = "Enter new name";
    private String surName = "Enter new surname";
    private String password = "Enter a password";


    private String position;
    private ArrayList<String> positions;

    @PostConstruct
    public void init() {

        positions = new ArrayList<String>();
        positions.add("Controller");
        positions.add("Technician");
        positions.add("Administrator");

    }

    public ArrayList<String> getPositions() {
        return positions;
    }

    public void setPositions(ArrayList<String> positions) {
        this.positions = positions;
    }


    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public CreateUserService getAdminService() {
        return adminService;
    }

    public void setAdminService(CreateUserService adminService) {
        this.adminService = adminService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void createUser(){
        if(position.equals("Technician")){
            adminService.createTechnician();
        } else if(position.equals("Controller")){
            adminService.createController();
        } else if (position.equals("Administrator")){
            adminService.createAdmin();
        }
    }


    public void seeTableOfUsers() throws IOException {

        FacesContext.getCurrentInstance().getExternalContext().redirect("tableUsers.xhtml");
    }
}

