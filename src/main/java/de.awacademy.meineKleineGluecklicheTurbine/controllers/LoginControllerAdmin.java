package de.awacademy.meineKleineGluecklicheTurbine.controllers;

import de.awacademy.meineKleineGluecklicheTurbine.services.LoginServiceAdmin;
import de.awacademy.meineKleineGluecklicheTurbine.services.LoginServiceController;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;

@Named
@RequestScoped
public class LoginControllerAdmin {
    @EJB
    private LoginServiceAdmin loginServiceAdmin;

    private String name = "your name";
    private String password = "your password";

    public void printLog(){
        System.out.println("TestLogin: Controller");
        System.out.println("username: "+ name + "\npassword: "+password);
    }

    public void loginCheck () throws IOException {

        if(loginServiceAdmin.loginCheckAdmins(name, password)){
            FacesContext.getCurrentInstance().getExternalContext().redirect("adminpageCreateUser.xhtml");
        } else {
            setName("your name");
            setPassword("your password");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
