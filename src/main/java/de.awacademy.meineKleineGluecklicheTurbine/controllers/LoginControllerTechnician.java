package de.awacademy.meineKleineGluecklicheTurbine.controllers;

import de.awacademy.meineKleineGluecklicheTurbine.services.LoginServiceTechnician;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;

@Named
@RequestScoped
public class LoginControllerTechnician {

    @EJB
    private LoginServiceTechnician loginServiceTechnician;

    private String name = "name";
    private String password = "password";

    public void printLog(){
        System.out.println("username: "+ name + "\npassword: "+password);
    }

    public void loginCheck () throws IOException {
        System.out.println("TestLogin:Technician");
        if(loginServiceTechnician.loginCheckTechnicians(name, password)){
            FacesContext.getCurrentInstance().getExternalContext().redirect("selectionpageTechnician.xhtml");
        } else {
            setName("your name");
            setPassword("your password");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
