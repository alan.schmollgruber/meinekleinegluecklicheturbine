package de.awacademy.meineKleineGluecklicheTurbine.controllers;

import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineModel;
import de.awacademy.meineKleineGluecklicheTurbine.models.UserModel;
import de.awacademy.meineKleineGluecklicheTurbine.services.DeleteUserService;
import de.awacademy.meineKleineGluecklicheTurbine.services.TurbineReportingService;
import de.awacademy.meineKleineGluecklicheTurbine.services.TurbineService;
import de.awacademy.meineKleineGluecklicheTurbine.services.UserService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.List;

@Named
@RequestScoped

public class TableUsersController {

    private List<UserModel> users;
//    private TurbineModel selectedTurbine;

//    @EJB
//    private TurbineReportingService turbineReportingService;

    @Inject
    private UserService userService;
    @Inject
    private DeleteUserService deleteUserService;

    @PostConstruct
    public void init(){
        users = userService.getAllUsers();
    }

    public List<UserModel> getUsers(){
        return users;
    }


    public void backToCreateuser() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect("adminpageCreateUser.xhtml");
    }

    public void deleteUser(Long idInput, String position) throws IOException{
        System.out.println("controller");
        deleteUserService.deleteSelectedUser(idInput, position);
        users = userService.getAllUsers();
        FacesContext.getCurrentInstance().getExternalContext().redirect("tableUsers.xhtml");

    }
}


