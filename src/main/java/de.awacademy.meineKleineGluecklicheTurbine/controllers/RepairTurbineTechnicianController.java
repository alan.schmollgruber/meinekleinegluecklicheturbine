package de.awacademy.meineKleineGluecklicheTurbine.controllers;

import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineTicketModel;
import de.awacademy.meineKleineGluecklicheTurbine.services.TurbineTicketRepairService;

import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

@Named
@ViewScoped
public class RepairTurbineTechnicianController implements Serializable {

    @EJB
    private TurbineTicketRepairService turbineTicketRepairService;

    private Long searchID;
    private int casing = 0;
    private int electronics = 0;
    private int gearbox = 0;
    private int generator = 0;
    private int rotor = 0;
    private int power = 0;
    private TurbineTicketModel turbineTicketModel = new TurbineTicketModel();

    //= tableTechnicianController.getLocalVar();

    public void sendConfirmationToController(){
//        turbineTicketModel.setCasing(casing);
//        turbineTicketModel.setElectronics(electronics);
//        turbineTicketModel.setGearbox(gearbox);
//        turbineTicketModel.setGenerator(generator);
//        turbineTicketModel.setRotor(rotor);
//        turbineTicketModel.setPower(power);
        //send information to the service

        String test = turbineTicketRepairService.updateValues();
        System.out.println("Controller repair test:"+test);

        try{
            FacesContext.getCurrentInstance().getExternalContext().redirect("tableTicketsTechnician.xhtml");
        } catch (IOException e){
            System.err.println("error in RepairTurbineTechnicianController: IOException");
        }
    }

    public Long getSearchID() {
        //System.out.println("Get inputSerialID: "+ searchID);
        return searchID;
    }

    public void setSearchID(Long searchID) {
        //System.out.println("Set inputSerialID: "+ searchID);
        this.searchID = searchID;
    }

    public int getCasing() {
        return casing;
    }

    public void setCasing(int casing) {
        this.casing = casing;
    }

    public int getElectronics() {
        return electronics;
    }

    public void setElectronics(int electronics) {
        this.electronics = electronics;
    }

    public int getGearbox() {
        return gearbox;
    }

    public void setGearbox(int gearbox) {
        this.gearbox = gearbox;
    }

    public int getGenerator() {
        return generator;
    }

    public void setGenerator(int generator) {
        this.generator = generator;
    }

    public int getRotor() {
        return rotor;
    }

    public void setRotor(int rotor) {
        this.rotor = rotor;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }


}
