package de.awacademy.meineKleineGluecklicheTurbine.controllers;

import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineReportModel;
import de.awacademy.meineKleineGluecklicheTurbine.reportingBusinessLogic.TurbineDiagnosticReporting;
import de.awacademy.meineKleineGluecklicheTurbine.services.TurbineReportingService;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static de.awacademy.meineKleineGluecklicheTurbine.reportingBusinessLogic.TurbineDiagnosticReporting.DEST;

//The class is named, since its attributes are accessed from the view component of the MVC-model.
@Named
//The class is RequestScoped, since the report is associated with the built-in request context required by the specification.
@RequestScoped
public class ReportingViewController {


    //Indicates a dependency on the local, no-interface, or remote view of an Enterprise JavaBean
    @EJB
    private TurbineReportingService turbineReportingService;



    private String serialID = "WindparkWittighausen2002 ";


    public void generateReport() {

        try {
            if (turbineReportingService.generateReport(serialID)) {

                //redirect to new site
                FacesContext.getCurrentInstance().getExternalContext().redirect("confirmationReportTicketCreated.xhtml");
            } else {

            }
        } catch (IOException e){
            System.err.println("ReportingViewController Error: IOException");
        }
    }


    public String getSerialID() {
        return serialID;
    }

    public void setSerialID(String serialID) {
        this.serialID = serialID;
    }
}
