package de.awacademy.meineKleineGluecklicheTurbine.controllers;

import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineModel;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineReportModel;
import de.awacademy.meineKleineGluecklicheTurbine.reportingBusinessLogic.TurbineDiagnosticReporting;
import de.awacademy.meineKleineGluecklicheTurbine.services.ReportingService;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static de.awacademy.meineKleineGluecklicheTurbine.reportingBusinessLogic.TurbineDiagnosticReporting.DEST;

//The class is named, since its attributes are accessed from the view component of the MVC-model.
@Named
//The class is RequestScoped, since the report is associated with the built-in request context required by the specification.
@RequestScoped
public class ReportingController {

    //Indicates a dependency on the local, no-interface, or remote view of an Enterprise JavaBean
    @EJB
    private ReportingService reportingService;

    private String serialID = "serialIDTest";
    //private String locationReport = "C:\\Users\\alans\\OneDrive\\Desktop\\Academy\\Woche7Zwischenprojekt\\meinekleinegluecklicheturbineReports";


    public void generateReport(){

        TurbineModel turbineModel = reportingService.findTurbine(serialID);

        if(turbineModel != null){
            try {
                //define timestamp related information
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
                LocalDateTime localDateTimeStamp = LocalDateTime.now();
                String dateTimeStamp = dateTimeFormatter.format(localDateTimeStamp);

                //create new turbineDiagnosticReporting-file instance
                TurbineDiagnosticReporting turbineDiagnosticReporting = new TurbineDiagnosticReporting("repairReport"+dateTimeStamp+".pdf");
                TurbineReportModel turbineReportModel = turbineDiagnosticReporting.getTurbineReportModel();
                File file = new File(DEST);
                file.getParentFile().mkdirs();

                //content input and pdf generation
                turbineDiagnosticReporting.importDocumentContent(localDateTimeStamp, turbineReportModel);
                turbineDiagnosticReporting.createPdf();

                //log
                System.out.println("\nTest generate report to path: ...\n..."+DEST);


                //redirect to new site
                FacesContext.getCurrentInstance().getExternalContext().redirect("reportView.xhtml");

            } catch (IOException e){
                System.out.println("IOException !");
            }
        } else {
            System.out.println("\nNO report to path: ...\n..."+DEST);
        }
    }


    public String getSerialID() {
        return serialID;
    }

    public void setSerialID(String serialID) {
        this.serialID = serialID;
    }
}
