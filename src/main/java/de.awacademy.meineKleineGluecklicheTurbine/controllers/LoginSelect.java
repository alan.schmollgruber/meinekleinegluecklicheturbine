package de.awacademy.meineKleineGluecklicheTurbine.controllers;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;


@Named
@ViewScoped
public class LoginSelect implements Serializable {

    private String position;
    private ArrayList<String> positions;

    @PostConstruct
    public void init() {

        positions = new ArrayList<String>();

        positions.add("Controller");
        positions.add("Technician");
        positions.add("Visitor");
        positions.add("Administrator");
    }

    public ArrayList<String> getPositions() {
        return positions;
    }

    public void setPositions(ArrayList<String> positions) {
        this.positions = positions;
    }


    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }


    public void redirecting() throws IOException {
        if(position.equals("Technician") )
            FacesContext.getCurrentInstance().getExternalContext().redirect("loginpageTechnician.xhtml");
        else if(position.equals("Controller"))
            FacesContext.getCurrentInstance().getExternalContext().redirect("loginpageController.xhtml");
        else if (position.equals("Visitor"))
            FacesContext.getCurrentInstance().getExternalContext().redirect("turbineDisplay.xhtml");
        else if (position.equals("Administrator"))
            FacesContext.getCurrentInstance().getExternalContext().redirect("loginAdmin.xhtml");

    }

    }