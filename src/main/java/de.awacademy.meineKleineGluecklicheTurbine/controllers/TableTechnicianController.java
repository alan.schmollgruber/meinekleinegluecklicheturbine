package de.awacademy.meineKleineGluecklicheTurbine.controllers;

import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineTicketModel;
import de.awacademy.meineKleineGluecklicheTurbine.services.DeleteTicketService;
import de.awacademy.meineKleineGluecklicheTurbine.services.TurbineTicketService;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class TableTechnicianController implements Serializable {

    private List<TurbineTicketModel> turbineTickets;
    private TurbineTicketModel selectedTurbineTicket;

    private Long searchID;

    @Inject
    private TurbineTicketService turbineTicketService;

    @Inject
    private DeleteTicketService deleteTicketService;

    @PostConstruct
    public void init(){
        turbineTickets = turbineTicketService.getAllTurbines();
    }


    public void repairTurbineTicket(TurbineTicketModel selectedTurbineTicket) {

        try {
            System.out.println("Repair the turbine with serialID: " + selectedTurbineTicket.getId());

            //redirect to new site
            searchID = selectedTurbineTicket.getId();
            FacesContext.getCurrentInstance().getExternalContext().redirect("solveTicketTurbine.xhtml?inputSerialID=" + searchID);
        } catch (IOException e){
            System.err.println("TableTechnicianController: repairTurbine-method: IOException encountered ");
        }
    }

    public void deleteTicket(Long id){
        deleteTicketService.deleteSelectedTicket(id);
        try{
            FacesContext.getCurrentInstance().getExternalContext().redirect("confirmationReportTicketSolved.xhtml");
        } catch (IOException e) {
            System.err.println("Redirect Test");
        }
    }

    public List<TurbineTicketModel> getTurbineTickets() {
        return turbineTickets;
    }

    public void setTurbineTickets(List<TurbineTicketModel> turbineTickets) {
        this.turbineTickets = turbineTickets;
    }

    public TurbineTicketModel getSelectedTurbineTicket() {
        return selectedTurbineTicket;
    }

    public void setSelectedTurbineTicket(TurbineTicketModel selectedTurbineTicket) {
        this.selectedTurbineTicket = selectedTurbineTicket;
    }

    public Long getSearchID() {
        return searchID;
    }

    public void setSearchID(Long searchID) {
        this.searchID = searchID;
    }
}
