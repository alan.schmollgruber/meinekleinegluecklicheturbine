package de.awacademy.meineKleineGluecklicheTurbine.controllers;
import de.awacademy.meineKleineGluecklicheTurbine.services.LoginService;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.SQLOutput;

@Named
@RequestScoped
public class LoginController implements Serializable {


    @EJB
    private LoginService loginService;

    private String name = "name";
    private String password = "password";

    public void printLog(){
        System.out.println("username: "+ name + "\npassword: "+password);
    }

    public void loginCheck () {
        System.out.println("TestLogin:Controler");
        System.out.println(loginService.loginCheck(name, password));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
