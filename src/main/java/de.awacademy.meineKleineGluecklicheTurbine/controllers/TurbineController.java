package de.awacademy.meineKleineGluecklicheTurbine.controllers;

import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineModel;
import de.awacademy.meineKleineGluecklicheTurbine.services.TurbineReportingService;
import de.awacademy.meineKleineGluecklicheTurbine.services.TurbineService;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class TurbineController implements Serializable{

    private List<TurbineModel> turbines;
    private TurbineModel selectedTurbine;
    private String turbineCoords;
    private String turbineData;

    @EJB
    private TurbineReportingService turbineReportingService;

    @Inject
    private TurbineService turbineService;

    @PostConstruct
    public void init(){
        turbines = turbineService.getAllTurbines();
    }

    public String getTurbineData() {
        turbineData = "";
        for (TurbineModel turbine : turbines) {
            String state = turbine.getState();
            int status = turbine.getStatus();
            String serialID = turbine.getSerialID();

            turbineData+= state + "," + status + "," + serialID + ";";
        }

        turbineData = turbineData.substring(0,turbineData.length()-1);
        return turbineData;

    }

    public String getTurbineCoordsByState(TurbineModel turbine) {
        /*String turbineCoordsBRLN = " ";
        String turbineCoordsBRMN = " ";
        String turbineCoordsBRNDNBRG = " ";
        String turbineCoordsBW = " ";
        String turbineCoordsBYRN = " ";
        String turbineCoordsHMBRG = " ";
        String turbineCoordsHSSN = " ";
        String turbineCoordsMCKLNBRG = " ";
        String turbineCoordsNDRSCHSN = " ";
        String turbineCoordsNRW = " ";
        String turbineCoordsRHNLDPFLZ = " ";
        String turbineCoordsSCHLSWG = " ";
        String turbineCoordsSCHSN = " ";
        String turbineCoordsSCHSNNHLT = " ";
        String turbineCoordsSRLND = " ";
        String turbineCoordsTHRNGN = " ";*/
        State state;
        String coordsMappedByState = "";


        final String[] stateSwitch = new String[]
                { "Berlin", "Bremen", "Brandenburg", "BadenWuerttemberg", "Hessen", "MacklenburgVorpommern", "Niedersachsen", "NordrheinWestfalen",
                        "RheinlandPfalz", "SchleswigHolstein", "Sachsen", "SachsenAnhalt", "Saarland", "Thueringen", "Hamburg", "Bayern"};

//        for (TurbineModel turbine : turbines) {
            String coordsDMS = turbine.getCoordinates();

            switch (turbine.getState()) {
                case "Berlin":
                    state = State.Berlin;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsBRLN+= coordsMappedByState + ";";
                    break;
                case "Bremen":
                    state = State.Bremen;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsBRMN+= coordsMappedByState + ";";
                    break;
                case "Brandenburg":
                    state = State.Brandenburg;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsBRNDNBRG+= coordsMappedByState + ";";
                    break;
                case "BadenWuerttemberg":
                    state = State.BadenWuerttemberg;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsBW+= coordsMappedByState + ";";
                    break;
                case "Hessen":
                    state = State.Hessen;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsHSSN+= coordsMappedByState + ";";
                    break;
                case "MacklenburgVorpommern":
                    state = State.MacklenburgVorpommern;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsMCKLNBRG+= coordsMappedByState + ";";
                    break;
                case "Niedersachsen":
                    state = State.Niedersachsen;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsNDRSCHSN+= coordsMappedByState + ";";
                    break;
                case "Nordrhein-Westfalen":
                    state = State.NordrheinWestfalen;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsNRW+= coordsMappedByState + ";";
                    break;
                case "RheinlandPfalz":
                    state = State.RheinlandPfalz;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsRHNLDPFLZ+= coordsMappedByState + ";";
                    break;
                case "Schleswig-Holstein":
                    state = State.SchleswigHolstein;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsSCHLSWG+= coordsMappedByState + ";";
                    break;
                case "Sachsen":
                    state = State.Sachsen;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsSCHSN+= coordsMappedByState + ";";
                    break;
                case "SachsenAnhalt":
                    state = State.SachsenAnhalt;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsSCHSNNHLT+= coordsMappedByState + ";";
                    break;
                case "Saarland":
                    state = State.Saarland;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsSRLND+= coordsMappedByState + ";";
                    break;
                case "Thueringen":
                    state = State.Thueringen;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsTHRNGN+= coordsMappedByState + ";";
                    break;
                case "Hamburg":
                    state = State.Hamburg;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsHMBRG+= coordsMappedByState + ";";
                    break;
                case "Bayern":
                    state = State.Bayern;
                    coordsMappedByState = mapCoordinatesByState(coordsDMS, state.xLimit, state.yLimit, state.xScale, state.yScale, state.xIntercept, state.yIntercept);
//                    turbineCoordsBYRN+= coordsMappedByState + ";";
                    break;
                default:
                    coordsMappedByState = "";

            }

        //}

      /*  turbineCoordsByState =
            turbineCoordsBRLN.substring(0,turbineCoordsBRLN.length()-1) +"%" +
            turbineCoordsBRMN.substring(0,turbineCoordsBRMN.length()-1) +"%" +
            turbineCoordsBRNDNBRG.substring(0,turbineCoordsBRNDNBRG.length()-1) +"%" +
            turbineCoordsBW.substring(0,turbineCoordsBW.length()-1) +"%" +
            turbineCoordsBYRN.substring(0,turbineCoordsBYRN.length()-1) +"%" +
            turbineCoordsHMBRG.substring(0,turbineCoordsHMBRG.length()-1) +"%" +
            turbineCoordsHSSN.substring(0,turbineCoordsHSSN.length()-1) +"%" +
            turbineCoordsMCKLNBRG.substring(0,turbineCoordsMCKLNBRG.length()-1) +"%" +
            turbineCoordsNDRSCHSN.substring(0,turbineCoordsNDRSCHSN.length()-1) +"%" +
            turbineCoordsNRW.substring(0,turbineCoordsNRW.length()-1) +"%" +
            turbineCoordsRHNLDPFLZ.substring(0,turbineCoordsRHNLDPFLZ.length()-1) +"%" +
            turbineCoordsSCHLSWG.substring(0,turbineCoordsSCHLSWG.length()-1) +"%" +
            turbineCoordsSCHSN.substring(0,turbineCoordsSCHSN.length()-1) +"%" +
            turbineCoordsSCHSNNHLT.substring(0,turbineCoordsSCHSNNHLT.length()-1) +"%" +
            turbineCoordsSRLND.substring(0,turbineCoordsSRLND.length()-1) +"%" +
            turbineCoordsTHRNGN.substring(0,turbineCoordsTHRNGN.length()-1) +"%";*/

//        turbineCoordsByState = turbineCoordsByState.substring(0,turbineCoordsByState.length()-1);


        return coordsMappedByState;
    }

    private enum State {
        Berlin(13.1143,52.6679, 3605,  5155, 0, 0),
        Bremen(8.66, 53.21, 4687, 10000, 0,0),
        Brandenburg(12.1566, 53.3476, 577, 899, 500, 200),
        BadenWuerttemberg(7.8025, 49.66, 741, 1076, 0, 180),
        Hessen(7.9493, 51.6256, 943, 1281, 0, 0 ),
        MacklenburgVorpommern(10.8326, 54.4549, 557, 920, 0, 150),
        Niedersachsen(6.7171, 53.88, 443, 697, 0, 0),
        NordrheinWestfalen(5.9772, 52.4713, 582, 950, 0,0),
        RheinlandPfalz(6.2112, 50.8177, 866, 1436, 0,0),
        SchleswigHolstein(8.7785, 54.9081, 732, 1264, 180,130),
        Sachsen(12.1764, 51.6681, 694, 1319, 0, 0 ),
        SachsenAnhalt(10.5881, 53.0210, 795, 1256, 0,0),
        Saarland(6.4608, 49.6429, 1724, 2250, 0,0),
        Thueringen(9.9027, 51.6462, 800, 1216, 0,0),
        Hamburg(9.3347, 53.6853, 2035, 8000, 0,0),
        Bayern(10.4268,50.5257, 483, 683, 400,0);

        State(double xLimit, double yLimit, double xScale, double yScale, int xIntercept, int yIntercept) {
            this.xLimit = xLimit;
            this.yLimit = yLimit;
            this.xScale = xScale;
            this.yScale = yScale;
            this.yIntercept = yIntercept;
            this.xIntercept = xIntercept;
        }

        private double xLimit;
        private double yLimit;
        private double xScale;
        private double yScale;
        private int yIntercept;
        private int xIntercept;

        public double getxLimit() {
            return xLimit;
        }

        public double getyLimit() {
            return yLimit;
        }

        public double getxScale() {
            return xScale;
        }

        public double getyScale() {
            return yScale;
        }

        public int getyIntercept() {
            return yIntercept;
        }

        public int getxIntercept() {
            return xIntercept;
        }
    }

    public String getTurbineCoords() {
        turbineCoords = "";
        for (TurbineModel turbine : turbines) {
            String coordsDMS = turbine.getCoordinates();

            String coordsMappedGeneral = mapCoordinates(coordsDMS);
            String coordsMappedByState = getTurbineCoordsByState(turbine);
            System.out.println(coordsMappedGeneral);
            System.out.println(coordsMappedByState);

            turbineCoords+= coordsMappedGeneral + "#" + coordsMappedByState + ";";
            System.out.println(turbineCoords);
        }

        turbineCoords = turbineCoords.substring(0,turbineCoords.length()-1);
        return turbineCoords;
    }

    private String mapCoordinatesByState(String turbineCoords, double xLimit, double yLimit, double xScale, double yScale, int xIntercept, int yIntercept) {

        double[] N_E_coordsDD = degminsecTOdecdeg(turbineCoords);

        double xCoord = (N_E_coordsDD[1] - xLimit) * xScale + xIntercept;
        double yCoord = (yLimit - N_E_coordsDD[0]) * yScale + yIntercept;

        return Math.round(xCoord) + "," + Math.round(yCoord);

    }

    private String mapCoordinates(String turbineCoords) {
        final double xLimit = 5.9389;
        final double yLimit = 54.9431;
        final double xScale = 1985/9.1627;
        final double yScale = 2632/7.66;
        final int yIntercept = 40;

        double[] N_E_coordsDD = degminsecTOdecdeg(turbineCoords);

        double xCoord = (N_E_coordsDD[1] - xLimit) * xScale;
        double yCoord = (yLimit - N_E_coordsDD[0]) * yScale + yIntercept;

        return Math.round(xCoord) + "," + Math.round(yCoord);

    }

    private double[] degminsecTOdecdeg(String turbineCoords) {
        String[] N_E_coordsDMS = turbineCoords.split(",");
        double[] N_E_coordsDD = new double[2];
        for (int i = 0; i<N_E_coordsDMS.length; i++) {
            String coords = N_E_coordsDMS[i].trim();

            int dEnd = coords.indexOf('d');
            int mStart = dEnd+2;
            int mEnd = coords.indexOf('m');
            int sStart = mEnd+2;
            int sEnd = coords.indexOf('s');

            double degrees = Double.parseDouble(coords.substring(0,dEnd));
            double minutes = Double.parseDouble(coords.substring(mStart,mEnd));
            double seconds = Double.parseDouble(coords.substring(sStart,sEnd));

            N_E_coordsDD[i] = degrees + (minutes/60) + (seconds/60/60);
        }

        return N_E_coordsDD;
    }

    public List<TurbineModel> getTurbines(){
       // turbines = turbineService.getAllTurbines();
        return turbines;
    }

    public void generateReport(TurbineModel turbineModel){
        System.out.println("New report function for: "+turbineModel.getSerialID() + " was executed.");

        try {
            boolean createReport = turbineReportingService.generateReport(turbineModel.getSerialID());
            if (createReport) {
                //create new ticket
                turbineReportingService.createNewTurbineTicket(turbineModel.getSerialID());

                //redirect to new site
                FacesContext.getCurrentInstance().getExternalContext().redirect("selectionpageController.xhtml");
            } else {

            }
        } catch (IOException e){
            System.err.println("ReportingViewController Error: IOException");
        }

    }

    public TurbineModel getSelectedTurbine() {
        return selectedTurbine;
    }

    public void setSelectedTurbine(TurbineModel selectedTurbine) {
        this.selectedTurbine = selectedTurbine;
    }
}
