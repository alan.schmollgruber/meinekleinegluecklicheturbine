package de.awacademy.meineKleineGluecklicheTurbine.mapper;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineTicketModel;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineReportModel;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
public class TurbineTicketMapper {

    public TurbineTicketMapper() {
    }

    public List<TurbineTicketModel> mapEntities(List<TurbineTicketEntity> turbineTicketEntities) {

        List<TurbineTicketModel> turbineReportModels = new ArrayList<>();

        for (TurbineTicketEntity turbine : turbineTicketEntities) {
            TurbineTicketModel turbineTicketModel = new TurbineTicketModel();

            //set all the
            turbineTicketModel.setId(turbine.getId());
            turbineTicketModel.setSerialID(turbine.getSerialID());
            turbineTicketModel.setCoordinates(turbine.getCoordinates());
            turbineTicketModel.setDistrict(turbine.getDistrict());
            turbineTicketModel.setCasing(turbine.getCasing());
            turbineTicketModel.setElectronics(turbine.getElectronics());
            turbineTicketModel.setGearbox(turbine.getGearbox());
            turbineTicketModel.setGenerator(turbine.getGenerator());
            turbineTicketModel.setRotor(turbine.getRotor());
            turbineTicketModel.setPower(turbine.getPower());
            turbineTicketModel.setStatus(turbine.getStatus());
            turbineTicketModel.setLastMantained(turbine.getLastMantained());
            turbineTicketModel.setState(turbine.getState());
            turbineTicketModel.setCreationTimestamp(turbine.getCreationTimestamp());

            turbineReportModels.add(turbineTicketModel);
        }

        return turbineReportModels;
    }

    public TurbineTicketModel mapEntity(TurbineTicketEntity turbineEntity) {

        TurbineTicketModel turbineTicketModel = new TurbineTicketModel();

        //set all the
        turbineTicketModel.setId(turbineEntity.getId());
        turbineTicketModel.setSerialID(turbineEntity.getSerialID());
        turbineTicketModel.setCoordinates(turbineEntity.getCoordinates());
        turbineTicketModel.setDistrict(turbineEntity.getDistrict());
        turbineTicketModel.setCasing(turbineEntity.getCasing());
        turbineTicketModel.setElectronics(turbineEntity.getElectronics());
        turbineTicketModel.setGearbox(turbineEntity.getGearbox());
        turbineTicketModel.setGenerator(turbineEntity.getGenerator());
        turbineTicketModel.setRotor(turbineEntity.getRotor());
        turbineTicketModel.setPower(turbineEntity.getPower());
        turbineTicketModel.setStatus(turbineEntity.getStatus());
        turbineTicketModel.setLastMantained(turbineEntity.getLastMantained());
        turbineTicketModel.setState(turbineEntity.getState());
        turbineTicketModel.setCreationTimestamp(turbineEntity.getCreationTimestamp());

        return turbineTicketModel;
    }

    public TurbineTicketEntity mapEntityReportToEntity (TurbineReportModel turbineReportModel, Date creationTimestamp) {

        TurbineTicketEntity turbineTicketEntity = new TurbineTicketEntity();

        //set all the
        turbineTicketEntity.setId(turbineReportModel.getId());
        turbineTicketEntity.setSerialID(turbineReportModel.getSerialID());
        turbineTicketEntity.setCoordinates(turbineReportModel.getCoordinates());
        turbineTicketEntity.setDistrict(turbineReportModel.getDistrict());
        turbineTicketEntity.setCasing(turbineReportModel.getCasing());
        turbineTicketEntity.setElectronics(turbineReportModel.getElectronics());
        turbineTicketEntity.setGearbox(turbineReportModel.getGearbox());
        turbineTicketEntity.setGenerator(turbineReportModel.getGenerator());
        turbineTicketEntity.setRotor(turbineReportModel.getRotor());
        turbineTicketEntity.setPower(turbineReportModel.getPower());
        turbineTicketEntity.setStatus(turbineReportModel.getStatus());
        turbineTicketEntity.setLastMantained(turbineReportModel.getLastMantained());
        turbineTicketEntity.setState(turbineReportModel.getState());
        turbineTicketEntity.setCreationTimestamp(creationTimestamp);

        return turbineTicketEntity;
    }

}


