package de.awacademy.meineKleineGluecklicheTurbine.mapper;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineModel;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineReportModel;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
public class TurbineReportMapper {

    public TurbineReportMapper() {
    }

    public List<TurbineReportModel> mapEntities(List<TurbineReportingEntity> turbineReportingEntities) {

        List<TurbineReportModel> turbineReportModels = new ArrayList<>();
        for (TurbineReportingEntity turbine : turbineReportingEntities) {
            TurbineReportModel turbineReportModel = new TurbineReportModel();

            //set all the
            turbineReportModel.setId(turbine.getId());
            turbineReportModel.setSerialID(turbine.getSerialID());
            turbineReportModel.setCoordinates(turbine.getCoordinates());
            turbineReportModel.setDistrict(turbine.getDistrict());
            turbineReportModel.setCasing(turbine.getCasing());
            turbineReportModel.setElectronics(turbine.getElectronics());
            turbineReportModel.setGearbox(turbine.getGearbox());
            turbineReportModel.setGenerator(turbine.getGenerator());
            turbineReportModel.setRotor(turbine.getRotor());
            turbineReportModel.setPower(turbine.getPower());
            turbineReportModel.setStatus(turbine.getStatus());
            turbineReportModel.setLastMantained(turbine.getLastMantained());
            turbineReportModel.setState(turbine.getState());

            turbineReportModels.add(turbineReportModel);
        }

        return turbineReportModels;
    }

    public TurbineReportModel mapEntity(TurbineReportingEntity turbineEntity) {

        TurbineReportModel turbineReportModel = new TurbineReportModel();

        //set all the
        turbineReportModel.setId(turbineEntity.getId());
        turbineReportModel.setSerialID(turbineEntity.getSerialID());
        turbineReportModel.setCoordinates(turbineEntity.getCoordinates());
        turbineReportModel.setDistrict(turbineEntity.getDistrict());
        turbineReportModel.setCasing(turbineEntity.getCasing());
        turbineReportModel.setElectronics(turbineEntity.getElectronics());
        turbineReportModel.setGearbox(turbineEntity.getGearbox());
        turbineReportModel.setGenerator(turbineEntity.getGenerator());
        turbineReportModel.setRotor(turbineEntity.getRotor());
        turbineReportModel.setPower(turbineEntity.getPower());
        turbineReportModel.setStatus(turbineEntity.getStatus());
        turbineReportModel.setLastMantained(turbineEntity.getLastMantained());
        turbineReportModel.setState(turbineEntity.getState());

        return turbineReportModel;
    }

    public TurbineTicketEntity mapEntityReportToEntity (TurbineReportModel turbineReportModel) {

        TurbineTicketEntity turbineTicketEntity = new TurbineTicketEntity();

        //set all the
        turbineTicketEntity.setId(turbineReportModel.getId());
        turbineTicketEntity.setSerialID(turbineReportModel.getSerialID());
        turbineTicketEntity.setCoordinates(turbineReportModel.getCoordinates());
        turbineTicketEntity.setDistrict(turbineReportModel.getDistrict());
        turbineTicketEntity.setCasing(turbineReportModel.getCasing());
        turbineTicketEntity.setElectronics(turbineReportModel.getElectronics());
        turbineTicketEntity.setGearbox(turbineReportModel.getGearbox());
        turbineTicketEntity.setGenerator(turbineReportModel.getGenerator());
        turbineTicketEntity.setRotor(turbineReportModel.getRotor());
        turbineTicketEntity.setPower(turbineReportModel.getPower());
        turbineTicketEntity.setStatus(turbineReportModel.getStatus());
        turbineTicketEntity.setLastMantained(turbineReportModel.getLastMantained());
        turbineTicketEntity.setState(turbineReportModel.getState());
        turbineTicketEntity.setCreationTimestamp(new Date());

        return turbineTicketEntity;
    }


}
