package de.awacademy.meineKleineGluecklicheTurbine.mapper;


import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineModel;
import de.awacademy.meineKleineGluecklicheTurbine.models.UserModel;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class TurbineMapper {

    public TurbineMapper(){

    }

    public List<TurbineModel> mapEntities(List<TurbineReportingEntity> turbineEntities) {

        List<TurbineModel> turbineModels = new ArrayList<>();

        for (TurbineReportingEntity turbine : turbineEntities) {
            TurbineModel turbineModel = new TurbineModel();

            turbineModel.setId(turbine.getId());
            turbineModel.setSerialID(turbine.getSerialID());
            turbineModel.setCoordinates(turbine.getCoordinates());
            turbineModel.setDistrict(turbine.getDistrict());
            turbineModel.setCasing(turbine.getCasing());
            turbineModel.setElectronics(turbine.getElectronics());
            turbineModel.setGearbox(turbine.getGearbox());
            turbineModel.setGenerator(turbine.getGenerator());
            turbineModel.setRotor(turbine.getRotor());
            turbineModel.setPower(turbine.getPower());
            turbineModel.setStatus(turbine.getStatus());
            turbineModel.setLastMantained(turbine.getLastMantained());
            turbineModel.setState(turbine.getState());

            turbineModels.add(turbineModel);
            //System.out.println("Turbine Mapper Test: "+turbineModels.size()+ " info: "+turbineModel.getSerialID());
        }
        return turbineModels;
    }

    public TurbineModel mapEntity(TurbineReportingEntity turbineEntity) {

        TurbineModel turbineModel = new TurbineModel();
        turbineModel.setId(turbineEntity.getId());
        turbineModel.setSerialID(turbineEntity.getSerialID());
        turbineModel.setCoordinates(turbineEntity.getCoordinates());
        turbineModel.setDistrict(turbineEntity.getDistrict());
        turbineModel.setCasing(turbineEntity.getCasing());
        turbineModel.setElectronics(turbineEntity.getElectronics());
        turbineModel.setGearbox(turbineEntity.getGearbox());
        turbineModel.setGenerator(turbineEntity.getGenerator());
        turbineModel.setRotor(turbineEntity.getRotor());
        turbineModel.setPower(turbineEntity.getPower());
        turbineModel.setStatus(turbineEntity.getStatus());
        turbineModel.setLastMantained(turbineEntity.getLastMantained());
        turbineModel.setState(turbineEntity.getState());

        return turbineModel;
    }

}
