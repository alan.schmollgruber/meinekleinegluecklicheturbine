package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.sql.SQLOutput;
import java.util.List;

@Stateless
public class TurbineRepository {

    private EntityManager entityManager;

    public TurbineRepository(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JEE_WEBBLOG_PERSISTENCE_UNIT");

        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public List<TurbineReportingEntity> findAll(){
        Query query = entityManager.createQuery("select turbine from TurbineReportingEntity turbine");
        List<TurbineReportingEntity> resultList = query.getResultList();

        //Test, when errors arise
        /*for(TurbineReportingEntity turbineReportingEntity:resultList){
            System.out.println("Test: Turbine Repository: " + turbineReportingEntity.getSerialID()+ " : " + turbineReportingEntity.getDistrict() );
        }*/

        return resultList;
    }

}
