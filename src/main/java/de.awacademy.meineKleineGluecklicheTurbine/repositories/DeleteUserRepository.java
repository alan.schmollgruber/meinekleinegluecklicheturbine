package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.AdminEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.models.UserModel;
import de.awacademy.meineKleineGluecklicheTurbine.services.UserService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class DeleteUserRepository {


    private static EntityManager entityManager;


    public void findUserToDelete(Long idInput,String position){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");
        entityManager = entityManagerFactory.createEntityManager();

        System.out.println("Id = " + idInput+ " Position = " + position);
        if(position.equals("Technician")) {
            Query query = entityManager.createQuery("select user from TechnicianEntity user where user.id= '" + idInput + "' ");

            List<TechnicianEntity> adminEntities = query.getResultList();
            for (TechnicianEntity user : adminEntities) {
                System.out.println("Id = " + user.getId()+ " Position = " + user.getPosition());
                System.out.println("name1 ="+user.getName());
                if (user.getId() == idInput) {
                    System.out.println("name =" + user.getName());

                    entityManager.getTransaction().begin();
                    entityManager.remove(user);
                    entityManager.getTransaction().commit();
                }
            }
        } else if(position.equals("Controller")){
            System.out.println("prequery");
            Query query = entityManager.createQuery("select user from ControllerEntity user where user.id= '" + idInput + "'");
            System.out.println("postquery");
            List<ControllerEntity> adminEntities = query.getResultList();
            System.out.println(adminEntities.get(0));
            for (ControllerEntity user : adminEntities) {
                System.out.println("Id = " + user.getId()+ " Position = " + user.getPosition());
                System.out.println("name1 ="+user.getName());
                if (user.getId() == idInput) {
                    System.out.println("name =" + user.getName());

                    entityManager.getTransaction().begin();
                    entityManager.remove(user);
                    entityManager.getTransaction().commit();
                }
            }
        }

    }

}
