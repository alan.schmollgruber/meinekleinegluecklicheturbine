package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Stateless
public class DeleteTicketRepositoryTest {

    private static EntityManager entityManager;

    public void findTicketToDelete(Long id){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");
        entityManager = entityManagerFactory.createEntityManager();

        TurbineTicketEntity removeTicketEntity = entityManager.find(TurbineTicketEntity.class,id);
        entityManager.getTransaction().begin();
        entityManager.remove(removeTicketEntity);
        entityManager.getTransaction().commit();
    }

}
