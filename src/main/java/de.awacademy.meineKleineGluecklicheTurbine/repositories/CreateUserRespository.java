package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.AdminEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Stateless
public class CreateUserRespository {

    private static EntityManager entityManager;

    public TechnicianEntity createUserTechnician ( String name, String surname,String password)  {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");
        entityManager = entityManagerFactory.createEntityManager();

        TechnicianEntity newTechnician = new TechnicianEntity();
        newTechnician.setName(name);
        newTechnician.setPassword(password);
        newTechnician.setSurname(surname);

        entityManager.getTransaction().begin();
        entityManager.persist(newTechnician);
        entityManager.getTransaction().commit();
        return newTechnician;
    }

    public ControllerEntity createUserController (String name, String surname,String password)  {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");
        entityManager = entityManagerFactory.createEntityManager();

        ControllerEntity newController = new ControllerEntity();
        newController.setName(name);
        newController.setPassword(password);
        newController.setSurname(surname);

        entityManager.getTransaction().begin();
        entityManager.persist(newController);
        entityManager.getTransaction().commit();
        return newController;
    }

    public AdminEntity createUserAdmin (String name, String surname,String password)  {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");
        entityManager = entityManagerFactory.createEntityManager();

        AdminEntity newAdmin = new AdminEntity();
        newAdmin.setName(name);
        newAdmin.setPassword(password);
        newAdmin.setSurname(surname);

        entityManager.getTransaction().begin();
        entityManager.persist(newAdmin);
        entityManager.getTransaction().commit();

        return newAdmin;
    }

}

