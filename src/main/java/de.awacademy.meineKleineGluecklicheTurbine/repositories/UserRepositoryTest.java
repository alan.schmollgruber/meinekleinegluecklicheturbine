package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class UserRepositoryTest {

    private EntityManager entityManager;

    public UserRepositoryTest(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");

    this.entityManager = entityManagerFactory.createEntityManager();
    }

    public List<TechnicianEntity> findAllTechnicians(){

        Query query = entityManager.createQuery("select user from TechnicianEntity user");
        List<TechnicianEntity> resultListTechnicians = query.getResultList();

        return resultListTechnicians;
    }

    public List<ControllerEntity> findAllControllers(){

        Query query = entityManager.createQuery("select user from ControllerEntity user");
        List<ControllerEntity> resultListController = query.getResultList();

        return resultListController;
    }

    public List<TechnicianEntity> joinTables(){

        Query query = entityManager.createQuery("select user from TechnicianEntity user");
        List<TechnicianEntity> resultList = query.getResultList();

        return resultList;
    }

}
