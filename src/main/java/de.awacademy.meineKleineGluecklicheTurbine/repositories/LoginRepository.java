package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class LoginRepository {

    private EntityManager entityManager;

    public LoginRepository(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JEE_WEBBLOG_PERSISTENCE_UNIT");

        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public List<TechnicianEntity> findAll(){

        Query query = entityManager.createQuery("select user from TechnicianEntity user");
        List<TechnicianEntity> resultList = query.getResultList();

        return resultList;
    }

    public TechnicianEntity loginCheck(String nameInput, String passwordInput){
        Query query = entityManager.createQuery("select user from TechnicianEntity user where user.name= '"+nameInput+"' AND user.password= '"+passwordInput+"'");

        List<TechnicianEntity> technicianEntity = query.getResultList();
        if(technicianEntity.size()>0){
            return technicianEntity.get(0);
        } else {
            System.out.println("No technician!");
           return null;
        }
    }

}
