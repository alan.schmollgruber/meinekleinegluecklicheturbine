package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class TurbineRepositoryTest {

    private EntityManager entityManager;

    public TurbineRepositoryTest(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");

        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public List<TurbineReportingEntity> findAll(){
        Query query = entityManager.createQuery("select turbine from TurbineReportingEntity turbine");
        List<TurbineReportingEntity> resultList = query.getResultList();

        //Test, when errors arise
        /*for(TurbineReportingEntity turbineReportingEntity:resultList){
            System.out.println("Test: Turbine Repository: " + turbineReportingEntity.getSerialID()+ " : " + turbineReportingEntity.getDistrict() );
        }*/

        return resultList;
    }

}
