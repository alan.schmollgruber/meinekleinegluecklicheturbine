package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class LoginRepositoryControllerTest {

    private EntityManager entityManager;

    public LoginRepositoryControllerTest(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");

        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public List<ControllerEntity> findAllControllers(){

        Query query = entityManager.createQuery("select user from ControllerEntity user");
        List<ControllerEntity> resultList = query.getResultList();

        return resultList;
    }

    public ControllerEntity loginCheckController(String nameInput, String passwordInput){
        Query query = entityManager.createQuery("select user from ControllerEntity user where user.name= '"+nameInput+"' AND user.password= '"+passwordInput+"'");

        List<ControllerEntity> controllerEntity = query.getResultList();
        if(controllerEntity.size()>0){
            return controllerEntity.get(0);
        } else {
            System.out.println("LoginRepositoryController: user looked for does not exist!");
            return null;
        }
    }
}
