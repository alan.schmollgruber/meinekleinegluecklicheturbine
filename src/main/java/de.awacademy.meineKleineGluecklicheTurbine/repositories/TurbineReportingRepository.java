package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class TurbineReportingRepository {

    private EntityManager entityManager;

    public TurbineReportingRepository(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");

        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public List<TurbineReportingEntity> findAll(){

        Query query = entityManager.createQuery("select turbine from TurbineReportingEntity turbine");
        List<TurbineReportingEntity> resultList = query.getResultList();

        return resultList;
    }


}
