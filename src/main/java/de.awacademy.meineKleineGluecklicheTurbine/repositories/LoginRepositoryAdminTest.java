package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.AdminEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class LoginRepositoryAdminTest {

    private EntityManager entityManager;

    public LoginRepositoryAdminTest(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");

        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public List<AdminEntity> findAllAdmins(){

        Query query = entityManager.createQuery("select user from AdminEntity user");
        List<AdminEntity> resultList = query.getResultList();

        return resultList;
    }

    public AdminEntity loginCheckAdmin(String nameInput, String passwordInput){
        Query query = entityManager.createQuery("select user from AdminEntity user where user.name= '"+nameInput+"' AND user.password= '"+passwordInput+"'");

        List<AdminEntity> adminEntities = query.getResultList();
        if(adminEntities.size()>0){
            return adminEntities.get(0);
        } else {
            System.out.println("LoginRepositoryController: user looked for does not exist!");
            return null;
        }
    }
}
