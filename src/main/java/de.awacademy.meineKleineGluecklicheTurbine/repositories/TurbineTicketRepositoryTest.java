package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class TurbineTicketRepositoryTest {

    private EntityManager entityManager;

    public TurbineTicketRepositoryTest(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public List<TurbineTicketEntity> findAll(){
        Query query = entityManager.createQuery("select ticket FROM TurbineTicketEntity ticket");
        List<TurbineTicketEntity> allTickets = query.getResultList();

        return allTickets;
    }

    //Add new TurbineTicket to its Database
    public void createNewTicket(TurbineTicketEntity turbineTicketEntity){

        entityManager.getTransaction().begin();
        //re-initialize the ticket
        TurbineTicketEntity newTurbineTicketEntity = new TurbineTicketEntity();
        newTurbineTicketEntity.setCasing(turbineTicketEntity.getCasing());
        System.out.println("Test: "+turbineTicketEntity.getCasing());

        newTurbineTicketEntity.setCoordinates(turbineTicketEntity.getCoordinates());
        //System.out.println("Test: "+turbineTicketEntity.getCoordinates());

        newTurbineTicketEntity.setCreationTimestamp(turbineTicketEntity.getCreationTimestamp());
        //System.out.println("Test: "+turbineTicketEntity);

        newTurbineTicketEntity.setSerialID(turbineTicketEntity.getSerialID());
        //System.out.println("Test: "+turbineTicketEntity.getSerialID());

        newTurbineTicketEntity.setDistrict(turbineTicketEntity.getDistrict());
        //System.out.println("Test: "+turbineTicketEntity.getDistrict());

        newTurbineTicketEntity.setElectronics(turbineTicketEntity.getElectronics());
        //System.out.println("Test: "+turbineTicketEntity.getElectronics());

        newTurbineTicketEntity.setGearbox(turbineTicketEntity.getGearbox());
        //System.out.println("Test: "+turbineTicketEntity.getGearbox());

        newTurbineTicketEntity.setGenerator(turbineTicketEntity.getGenerator());
        //System.out.println("Test: "+turbineTicketEntity.getGenerator());

        newTurbineTicketEntity.setRotor(turbineTicketEntity.getRotor());
        //System.out.println("Test: "+turbineTicketEntity.getRotor());

        newTurbineTicketEntity.setPower(turbineTicketEntity.getPower());
        //System.out.println("Test: "+turbineTicketEntity.getPower());

        newTurbineTicketEntity.setStatus(turbineTicketEntity.getStatus());
        //System.out.println("Test: "+turbineTicketEntity.getStatus());

        newTurbineTicketEntity.setLastMantained(turbineTicketEntity.getLastMantained());
        //System.out.println("Test: "+turbineTicketEntity.getLastMantained());

        newTurbineTicketEntity.setState(turbineTicketEntity.getState());
        //System.out.println("Test: "+turbineTicketEntity.getState());

        newTurbineTicketEntity.setIdTurbine(turbineTicketEntity.getId());
        //System.out.println("Test: "+turbineTicketEntity.getState());

        entityManager.persist(newTurbineTicketEntity);
        entityManager.getTransaction().commit();
    }

}
