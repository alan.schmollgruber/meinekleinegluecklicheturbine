package de.awacademy.meineKleineGluecklicheTurbine.repositories;


import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class LoginRepositoryTechnicianTest {

    private EntityManager entityManager;

    public LoginRepositoryTechnicianTest(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("TEST");

        this.entityManager = entityManagerFactory.createEntityManager();
    }

    public List<TechnicianEntity> findAllTechnicians(){

        Query query = entityManager.createQuery("select user from TechnicianEntity user");
        List<TechnicianEntity> resultList = query.getResultList();

        return resultList;
    }

    public TechnicianEntity loginCheckTechnicians(String nameInput, String passwordInput){
        Query query = entityManager.createQuery("select user from TechnicianEntity user where user.name= '"+nameInput+"' AND user.password= '"+passwordInput+"'");

        List<TechnicianEntity> technicianEntity = query.getResultList();
        if(technicianEntity.size()>0){
            return technicianEntity.get(0);
        } else {
            System.out.println("No technician!");
            return null;
        }
    }
}
