package de.awacademy.meineKleineGluecklicheTurbine.repositories;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

@Stateless
public class UpdateTicketRepository {

    private static EntityManager entityManager;

    public Long updateTicketValues(Long searchID, int casing, int electronics, int gearbox, int generator, int rotor, int power, int status, int lastMantained, boolean repaired){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JEE_WEBBLOG_PERSISTENCE_UNIT");
        entityManager = entityManagerFactory.createEntityManager();

        TurbineTicketEntity ticketEntity = entityManager.find(TurbineTicketEntity.class,searchID);
        Long databaseId = ticketEntity.getIdTurbine();

        entityManager.getTransaction().begin();
        ticketEntity.setCasing(casing);
        ticketEntity.setElectronics(electronics);
        ticketEntity.setGearbox(gearbox);
        ticketEntity.setGenerator(generator);
        ticketEntity.setRotor(rotor);
        ticketEntity.setPower(power);
        ticketEntity.setStatus(status);
        ticketEntity.setLastMantained(lastMantained);
        ticketEntity.setRepaired(repaired);

        entityManager.getTransaction().commit();
        System.out.println("Test reposritory: "+ ticketEntity.getIdTurbine()+" : " + ticketEntity.getCasing());

        return databaseId;
    }

}
