package de.awacademy.meineKleineGluecklicheTurbine.reportingBusinessLogic;


import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.*;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineReportModel;

/**
 * Creates a simple PDF with text and images.
 */
public class TurbineDiagnosticReporting {

    /** The resulting PDF. */
    //public static String DEST = "reports/";
//    public static String PATH_FOLDER = "C:\\Users\\alans\\OneDrive\\Desktop\\Academy\\Woche7Zwischenprojekt\\meinekleinegluecklicheturbineReports\\reports\\";
//    public static String PATH_FOLDER = "C:\\Users\\monic\\Desktop\\reports\\";
    public static String PATH_FOLDER = "C:\\Users\\Dennis\\Desktop\\reports\\";
    public static String DEST = PATH_FOLDER;

    /**
     * Constructor
     * @param nameNewDocument Name of the newly generated pdf.
     */
    public TurbineDiagnosticReporting(String nameNewDocument) {
        DEST = PATH_FOLDER;
        DEST += nameNewDocument;
    }

    private LocalDateTime reportDateTimeStamp;
    private String turbineSerialId;
    private int turbineCapacity;
    private TurbineReportModel turbineReportModel;

    public void importDocumentContent(LocalDateTime reportDateTimeStamp, TurbineReportModel turbineReportModel){
        this.reportDateTimeStamp = reportDateTimeStamp;
        this.turbineReportModel = turbineReportModel;
    }

    /**
     * Function is used to define the content in the pdf-report.
     */
    public Document generateDocumentContent(Document document){


        document.setMargins(20, 20, 20, 20);


        document.add(
                new Paragraph()
                        .setFontSize(12)
                        .add(new Text("The report was generated at:"+getReportDateTimeStamp()))
        );

        try{
            ImageData data = ImageDataFactory.create(PATH_FOLDER+"KGTlogo.jpg");
            Image img = new Image(data);
            document.add(img);
        } catch (IOException e){
            System.err.println("Error with report image");
        }


        document.add(
                new Paragraph()
                        .setFontSize(32)
                        .add(new Text("KGT-automatic-report"))
        );
        document.add(
                new Paragraph()
                        .setFontSize(20)
                        .add(new Text("New Ticket for turbine: "+ turbineReportModel.getSerialID()))
        );



        document.add(
                new Paragraph()
                        .setFontSize(12)
                        .add(new Text("Current capacity of the turbine: "+ turbineReportModel.getStatus()+"%."))
        );

        document.add(
                new Paragraph()
                        .setFontSize(12)
                        .add(new Text("Location: "+ turbineReportModel.getDistrict()+", "+
                                                    turbineReportModel.getState()+" ("+
                                                    turbineReportModel.getCoordinates()+")."))
        );

        // Creating a table
        float [] pointColumnWidths = {150F, 150F};
        Table table = new Table(pointColumnWidths);

        // Adding cells to the table
        table.addCell(new Cell().add("Turbine-Part"));
        table.addCell(new Cell().add("Working Capacity"));
        table.addCell(new Cell().add("Casing"));
        String casing = Integer.toString(turbineReportModel.getCasing());
        table.addCell(new Cell().add(casing+"%"));

        table.addCell(new Cell().add("electronics"));
        String electronics = Integer.toString(turbineReportModel.getElectronics());
        table.addCell(new Cell().add(electronics+"%"));

        table.addCell(new Cell().add("gearbox"));
        String gearbox = Integer.toString(turbineReportModel.getGearbox());
        table.addCell(new Cell().add(gearbox+"%"));

        table.addCell(new Cell().add("rotor"));
        String rotor = Integer.toString(turbineReportModel.getRotor());
        table.addCell(new Cell().add(rotor+"%"));

        table.addCell(new Cell().add("power"));
        String power = Integer.toString(turbineReportModel.getPower());
        table.addCell(new Cell().add(power+"%"));

        table.addCell(new Cell().add("status"));
        String status = Integer.toString(turbineReportModel.getPower());
        table.addCell(new Cell().add(status+"%"));

        // Adding Table to document
        document.add(table);

        document.add(
                new Paragraph()
                        .setFontSize(12)
                        .add(new Text("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. "))
        );

        return document;
    }

    /**
     * Creates the pdf.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void createPdf() throws IOException {
        // step 1
        PdfDocument pdfDocument = new PdfDocument(new PdfWriter(DEST));
        pdfDocument.setDefaultPageSize(PageSize.A4.rotate());
        // step 2 - create new document instance
        Document document = new Document(pdfDocument);
        // step 3 - update document content
        document = generateDocumentContent(document);

        // step 4
        document.close();
    }

    public LocalDateTime getReportDateTimeStamp() {
        return reportDateTimeStamp;
    }

    public void setReportDateTimeStamp(LocalDateTime reportDateTimeStamp) {
        this.reportDateTimeStamp = reportDateTimeStamp;
    }

    public String getTurbineSerialId() {
        return turbineSerialId;
    }

    public void setTurbineSerialId(String turbineSerialId) {
        this.turbineSerialId = turbineSerialId;
    }

    public int getTurbineCapacity() {
        return turbineCapacity;
    }

    public void setTurbineCapacity(int turbineCapacity) {
        this.turbineCapacity = turbineCapacity;
    }

    public TurbineReportModel getTurbineReportModel() {
        return turbineReportModel;
    }
}
