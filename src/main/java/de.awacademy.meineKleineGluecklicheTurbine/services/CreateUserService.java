package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.controllers.AdminController;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.CreateUserRespository;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class CreateUserService {

//    private String name = "your name";
////    private String surname = "your surname";
////    private String password = "your password";


    @Inject
    private AdminController adminController;
    @Inject
    private CreateUserRespository createUserRespository;

    public void createTechnician(){
        createUserRespository.createUserTechnician(adminController.getName(), adminController.getPassword(), adminController.getSurName());

    }

    public void createController(){
        createUserRespository.createUserController(adminController.getName(), adminController.getPassword(), adminController.getSurName());

    }
    public void createAdmin(){
        createUserRespository.createUserAdmin(adminController.getName(), adminController.getPassword(), adminController.getSurName());

    }
}
