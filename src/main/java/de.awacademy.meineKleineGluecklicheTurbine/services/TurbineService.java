package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;
import de.awacademy.meineKleineGluecklicheTurbine.mapper.TurbineMapper;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineModel;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineRepository;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ApplicationScoped
public class TurbineService {

    @EJB
    private TurbineRepository turbineRepository;

    @EJB
    private TurbineMapper turbineMapper;

    public List<TurbineModel> getAllTurbines(){

        List<TurbineReportingEntity> turbineEntities = turbineRepository.findAll();
        //System.out.println("Test Turbine Service: "+ turbineEntities.size() + " info: "+turbineEntities.get(5).getSerialID());

        return turbineMapper.mapEntities(turbineEntities);

    }


}
