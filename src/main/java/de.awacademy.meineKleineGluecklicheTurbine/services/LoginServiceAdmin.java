package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.AdminEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.models.UserModel;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryAdmin;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryController;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
@Stateless
public class LoginServiceAdmin {
    @Inject
    private LoginRepositoryAdmin loginRepositoryAdmin;

    public List<UserModel> getAllControllers(){
        List<AdminEntity> users = loginRepositoryAdmin.findAllAdmins();

        List<UserModel> userModels = new ArrayList<>();
        for(AdminEntity user:users){
            UserModel userModel = new UserModel();
            userModel.setId(user.getId());
            userModel.setName(user.getName());
            userModel.setSurname(user.getSurname());

            userModels.add(userModel);
        }

        return userModels;
    }

    public boolean loginCheckAdmins(String name, String password) {
        System.out.println("TestLogin:Service");
        AdminEntity adminEntity = loginRepositoryAdmin.loginCheckAdmin(name,password);

        if(adminEntity!=null){
            System.out.println("LoginServiceAdministrator Test positive: " +adminEntity.getName()+ " " +adminEntity.getSurname());
            return true;
        } else {
            System.out.println("LoginServiceAdministrator Test false: The user is not contained in the database.");
            return false;
        }
    }
}
