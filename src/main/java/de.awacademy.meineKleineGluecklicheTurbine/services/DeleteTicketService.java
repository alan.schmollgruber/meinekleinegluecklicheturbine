package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.repositories.DeleteTicketRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class DeleteTicketService {

    @Inject
    private DeleteTicketRepository deleteTicketRepository;

    public void deleteSelectedTicket(Long id){
        deleteTicketRepository.findTicketToDelete(id);
    }


}
