package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.models.UserModel;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.UserRepository;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class UserService {

    @EJB
    private UserRepository userRepository;

    public List<UserModel> getAllUsers(){
        List<TechnicianEntity> technicians = userRepository.findAllTechnicians();
        List<ControllerEntity> controllers = userRepository.findAllControllers();


        List<UserModel> userModels = new ArrayList<>();
        for(TechnicianEntity user:technicians){
            UserModel userModel = new UserModel();
            userModel.setId(user.getId());
            userModel.setName(user.getName());
            userModel.setSurname(user.getSurname());
            userModel.setPosition(user.getPosition());
            userModel.setPassword(user.getPassword());

            userModels.add(userModel);
        }

        for(ControllerEntity user2:controllers){
            UserModel userModel = new UserModel();
            userModel.setId(user2.getId());
            userModel.setName(user2.getName());
            userModel.setSurname(user2.getSurname());
            userModel.setPosition(user2.getPosition());
            userModel.setPassword(user2.getPassword());

            userModels.add(userModel);
        }

        return userModels;
    }


}
