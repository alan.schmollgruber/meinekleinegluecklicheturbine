package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;
import de.awacademy.meineKleineGluecklicheTurbine.mapper.TurbineReportMapper;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineReportModel;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineReportingRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineTicketRepository;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Stateless
public class TurbineNewTicketService {

    @Inject
    private TurbineTicketRepository turbineTicketRepository;

    public void newTurbineTicketIntoDB(TurbineTicketEntity turbineTicketEntity){
        System.out.println("Insert ticket in DB"+ turbineTicketEntity.getSerialID());
        turbineTicketRepository.createNewTicket(turbineTicketEntity);
    }

}
