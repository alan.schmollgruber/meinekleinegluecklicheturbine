package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.models.UserModel;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryTechnician;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class LoginServiceTechnician {

    @Inject
    private LoginRepositoryTechnician loginRepositoryTechnician;

    public List<UserModel> getAllTechnicians(){

        List<TechnicianEntity> users = loginRepositoryTechnician.findAllTechnicians();

        List<UserModel> userModels = new ArrayList<>();
        for(TechnicianEntity user:users){
            UserModel userModel = new UserModel();
            userModel.setId(user.getId());
            userModel.setName(user.getName());
            userModel.setSurname(user.getSurname());

            userModels.add(userModel);
        }

        return userModels;
    }

    public boolean loginCheckTechnicians(String name, String password) {
        System.out.println("TestLogin:Service");
        TechnicianEntity technicianEntity = loginRepositoryTechnician.loginCheckTechnicians(name,password);

        if(technicianEntity!=null){
            System.out.println("LoginServiceTechnician Test positive: " +technicianEntity.getName()+ " " +technicianEntity.getSurname());
            return true;
        } else {
            System.out.println("LoginServiceTechnician Test false: The user is not contained in the database.");
            return false;
        }
    }

}
