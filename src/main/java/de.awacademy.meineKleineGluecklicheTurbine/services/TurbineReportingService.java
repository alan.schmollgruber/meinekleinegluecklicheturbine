package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;
import de.awacademy.meineKleineGluecklicheTurbine.mapper.TurbineReportMapper;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineReportModel;
import de.awacademy.meineKleineGluecklicheTurbine.reportingBusinessLogic.TurbineDiagnosticReporting;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineReportingRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineTicketRepository;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import static de.awacademy.meineKleineGluecklicheTurbine.reportingBusinessLogic.TurbineDiagnosticReporting.DEST;

@Stateless
public class TurbineReportingService {

    @Inject
    private TurbineReportingRepository turbineReportingRepository;

    @Inject
    private TurbineReportMapper turbineReportMapper;

    @Inject
    private TurbineNewTicketService turbineNewTicketService;


    public TurbineReportModel findTurbine(String serialId){

        List<TurbineReportingEntity> allTurbines = turbineReportingRepository.findAll();

        for(TurbineReportingEntity turbineReportingEntity:allTurbines){

            String entitySerial = turbineReportingEntity.getSerialID().substring(1).trim();
            String entitySerial2 = turbineReportingEntity.getSerialID().trim();

            boolean serialEquality = entitySerial.equals(serialId.trim()) || entitySerial2.equals(serialId);
            //System.out.println("Test Reporting Entity List: " +serialEquality + " " + serialId + " "+ entitySerial + " "+ entitySerial2);
            if(serialEquality){
                return turbineReportMapper.mapEntity(turbineReportingEntity);
            }
        }
        return null;
    }

    public boolean generateReport(String serialID){

        TurbineReportModel turbineReportModel = findTurbine(serialID);
        if(turbineReportModel != null){
            try {
                //define timestamp related information
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss");
                LocalDateTime localDateTimeStamp = LocalDateTime.now();
                Date date = new Date();

                String dateTimeStamp = dateTimeFormatter.format(localDateTimeStamp);

                //create new turbineDiagnosticReporting-file instance
                TurbineDiagnosticReporting turbineDiagnosticReporting = new TurbineDiagnosticReporting("repairReport"+dateTimeStamp+".pdf");
                File file = new File(DEST);
                file.getParentFile().mkdirs();

                //content input and pdf generation
                turbineDiagnosticReporting.importDocumentContent(localDateTimeStamp, turbineReportModel);
                turbineDiagnosticReporting.createPdf();

                //log
                System.out.println("\nTest generate report to path: ...\n..."+DEST);


                /*//redirect to new site
                FacesContext.getCurrentInstance().getExternalContext().redirect("reporting.xhtml")*/;

                //Create new TurbineTicket
                //TurbineTicketEntity turbineTicketEntity = turbineReportMapper.mapEntityReportToEntity(turbineReportModel, date);
                //turbineTicketRepository.createNewTicket(turbineTicketEntity);

            } catch (IOException e){
                System.out.println("IOException !");
            }


            return true;
        } else {
            System.out.println("\nNO report to path: ...\n..." + DEST);
            return false;
        }
    }

    public void createNewTurbineTicket(String serialID){
        TurbineReportModel turbineReportModel = findTurbine(serialID);
        TurbineTicketEntity turbineTicketEntity = turbineReportMapper.mapEntityReportToEntity(turbineReportModel);
        turbineNewTicketService.newTurbineTicketIntoDB(turbineTicketEntity);
    }

}
