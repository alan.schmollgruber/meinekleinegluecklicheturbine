package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.models.UserModel;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class LoginService {

    @Inject
    private LoginRepository loginRepository;

    public List<UserModel> getAllUsers(){
        List<TechnicianEntity> users = loginRepository.findAll();

        List<UserModel> userModels = new ArrayList<>();
        for(TechnicianEntity user:users){
            UserModel userModel = new UserModel();
            userModel.setId(user.getId());
            userModel.setName(user.getName());
            userModel.setSurname(user.getSurname());

            userModels.add(userModel);
        }

        return userModels;
    }

    public String loginCheck(String name, String password) {
        System.out.println("TestLogin:Service");
        TechnicianEntity technicianEntity = loginRepository.loginCheck(name,password);

        if(technicianEntity!=null){
            return "Hallo " +technicianEntity.getName()+ " " +technicianEntity.getSurname();
        } else {
            return "No shit in Daatabaaase... PEZDEC!";
        }
    }


}
