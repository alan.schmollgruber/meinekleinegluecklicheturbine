package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;
import de.awacademy.meineKleineGluecklicheTurbine.mapper.TurbineMapper;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineModel;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class ReportingService {


    @Inject
    private TurbineRepository turbineRepository;

    @Inject
    private TurbineMapper turbineMapper;

    public TurbineModel findTurbine(String serialId){

        List<TurbineReportingEntity> allTurbines = turbineRepository.findAll();

        for(TurbineReportingEntity turbineEntity:allTurbines){
            if(turbineEntity.getSerialID().equals(serialId)){
                return turbineMapper.mapEntity(turbineEntity);
            }
        }
        return null;
    }

}
