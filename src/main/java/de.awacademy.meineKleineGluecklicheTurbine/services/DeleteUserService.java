package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.repositories.CreateUserRespository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.DeleteUserRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class DeleteUserService {

    @Inject
    private DeleteUserRepository deleteUserRespository;

    public void deleteSelectedUser(Long idInput, String position){
        System.out.println("service");
        deleteUserRespository.findUserToDelete(idInput, position);
    }
}
