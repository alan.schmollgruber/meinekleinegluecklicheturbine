package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineReportingEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TurbineTicketEntity;
import de.awacademy.meineKleineGluecklicheTurbine.mapper.TurbineTicketMapper;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineReportModel;
import de.awacademy.meineKleineGluecklicheTurbine.models.TurbineTicketModel;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineReportingRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.TurbineTicketRepository;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.util.List;

@Named
@ApplicationScoped
public class TurbineTicketService {

    @EJB
    private TurbineTicketRepository turbineTicketRepository;

    @EJB
    private TurbineTicketMapper turbineTicketMapper;

    public List<TurbineTicketModel> getAllTurbines(){

        List<TurbineTicketEntity> turbineTicketModels = turbineTicketRepository.findAll();

        return turbineTicketMapper.mapEntities(turbineTicketModels);
    }



}
