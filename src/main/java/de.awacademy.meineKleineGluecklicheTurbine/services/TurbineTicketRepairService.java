package de.awacademy.meineKleineGluecklicheTurbine.services;


import de.awacademy.meineKleineGluecklicheTurbine.controllers.RepairTurbineTechnicianController;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.UpdateTicketRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

@Stateless
public class TurbineTicketRepairService {

    @Inject
    private RepairTurbineTechnicianController repairTurbineTechnicianController;

    @Inject
    private UpdateTicketRepository updateTicketsRepository;

    public Long updateValueTicket(){
        System.out.println("TurbineTicketRepairService Test: "+repairTurbineTechnicianController.getSearchID()+ " casing: "+repairTurbineTechnicianController.getCasing());


        Long searchID =  repairTurbineTechnicianController.getSearchID();
        int casing = repairTurbineTechnicianController.getCasing();
        int electronics = repairTurbineTechnicianController.getElectronics();
        int gearbox = repairTurbineTechnicianController.getGearbox();
        int generator = repairTurbineTechnicianController.getGenerator();
        int rotor = repairTurbineTechnicianController.getRotor();
        int power = repairTurbineTechnicianController.getPower();
        int status = (int)Math.floor((casing+electronics+gearbox+generator+rotor+power)/6);
        int lastMantained = 0;
        boolean repaired = true;

        Long repoTest = updateTicketsRepository.updateTicketValues(
                searchID,
                casing,
                electronics,
                gearbox,
                generator,
                rotor,
                power,
                status,
                lastMantained,
                repaired);

        return repoTest;
    }


    public String updateValues(){
        Long idDatabase =  updateValueTicket();
        System.out.println("database");
        return "Service: good";
    }


}
