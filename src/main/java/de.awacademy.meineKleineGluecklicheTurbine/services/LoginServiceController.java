package de.awacademy.meineKleineGluecklicheTurbine.services;

import de.awacademy.meineKleineGluecklicheTurbine.entities.ControllerEntity;
import de.awacademy.meineKleineGluecklicheTurbine.entities.TechnicianEntity;
import de.awacademy.meineKleineGluecklicheTurbine.models.UserModel;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepository;
import de.awacademy.meineKleineGluecklicheTurbine.repositories.LoginRepositoryController;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class LoginServiceController {

    @Inject
    private LoginRepositoryController loginRepositoryController;

    public List<UserModel> getAllControllers(){
        List<ControllerEntity> users = loginRepositoryController.findAllControllers();

        List<UserModel> userModels = new ArrayList<>();
        for(ControllerEntity user:users){
            UserModel userModel = new UserModel();
            userModel.setId(user.getId());
            userModel.setName(user.getName());
            userModel.setSurname(user.getSurname());

            userModels.add(userModel);
        }

        return userModels;
    }

    public boolean loginCheckControllers(String name, String password) {
        System.out.println("TestLogin:Service");
        ControllerEntity controllerEntity = loginRepositoryController.loginCheckController(name,password);

        if(controllerEntity!=null){
            System.out.println("LoginServiceController Test positive: " +controllerEntity.getName()+ " " +controllerEntity.getSurname());
            return true;
        } else {
            System.out.println("LoginServiceController Test false: The user is not contained in the database.");
            return false;
        }
    }
}
